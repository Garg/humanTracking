/*! @file */
#ifndef TRACKER_H
#define TRACKER_H

#include <sc_tracking2/trackFunctions.h>

/*!  0 for enabling a waitkey after color segmentation call.*/
int WAITKEY=1;

bool startFlag = false;
int templateCount = 0;
int kalmanCount = 0;
int frameNumber = 0;

Rect box_selection;
bool drawing_box = false;                       // Box selection parameters
bool rect_drawn = false;
bool drawing_poly = true;
bool poly_drawn = false;

int partSelected = 0;                           // number of parts selected in the body, could be three or two (no head)
vector<Point2f> partPoints, bodyPointsSelected; // stored partPoints, bodyPoints in mouse rectangle callback
bool pointSelectionDone = false;                // to check when points selection completes

/*!
  @brief Structure corresponding to a human template.
  */
struct HumanTemplate{
    int humanTemplateIndex ;
    RotatedRect hEllipse,tEllipse,lEllipse,haEllipse;
    Mat humanDescriptors;
    Mat humanImage;
    Rect humanBox;
    vector<Point2f> humanKeypoint;
    Mat humanStickImage;
    Mat surfImage;
    Mat colorHumanImage;
    double h2t,t2l;
    double scaleFactor;
};

/*!
  @brief Template tree structure.
  */
struct Tree
{
    //char data
    int data;
    Tree *left;
    Tree *right;
    Tree *parent;
};

struct Tree *newTreeNode(int data)
{
    Tree *node = new Tree;
    node->data = data;
    node->left = NULL;
    node->right = NULL;
    node->parent = NULL;

    return node;
}

/*!
  @brief A tracker class object can be initiated for multiple human tracking. Currently only single human
  tracking functionality is available. The class instance is created in the main function.
  */
class Tracker{
protected:
private:

public:

    VideoWriter writer;
    bool updateFilter;
    bool selectTemplate;                // flag if template has to be selected or not
    ofstream treecheckFile;
    int totalTemplates;
    bool goodTemplateSearch;
    double scale ;
    vector<int> extractPoints;
    Mat videoImage;
    Point2f pCenter,pCp;
    Rect finalWindow,pBox;
    HumanTemplate *humanTemplates;

    // -------------------------------------------
    // Connected Components Parameters
    // -------------------------------------------

    int ffillMode ;
    int loDiff, upDiff;

    // thresholds for individual connected components in any body part
    vector<int> areaVector;


    // -------------------------------------------
    // General Variables
    // -------------------------------------------

    Rect currentImgRect;
    Rect searchWindow;
    bool humanDetectedMRF;
    Rect boundingRectMRF;
    //    vector<int> occLusionFlagHistory;
    ofstream dataFile;
    ofstream descriptorFile;
    Mat_<int> adjMat;
    // timer file  - FrameNumber    templateSearchTime  InitSurf  SURFmatch    predictor   imageProc   colorPart   final
    ofstream timerFile2;
    EM emModel1, emModel2, emModel3, emModel4;
    bool exit_flag;
    bool surfMatched;
    Mat clustersOrg;

    Mat currentLabels;
    Mat currentsamples;

    vector<KeyPoint> currentImgKeypoint,prevImgKeypoint;
    Mat currentImgDesc,prevImgDesc;
    vector<Point2f> currentImgPoints;

    Mat imgsrc;
    Mat firstFrame,firstImg;
    Mat prevImg,currentImg;
    Mat prevImgROI,currentImgROI;
    Mat colorImg2;
    Mat currentMatchDesc;
    vector<Point2f> currentImgMatchPoint;

    // Aspect ration
    double AR;
    Point2f centerSurf;

    Rect box,boxOrg,B1,B2,B3;
    VideoCapture cap;

    vector<Vec3b> colors;
    Mat colorCurrentImg;
    Mat colorCurrentImgROI,hsvCurrentImgROI;

    cv::SURF mySURF;
    Ptr<DescriptorMatcher> descriptorMatcher;
    int mactherFilterType;

    Mat surfShowImage;

    double avgDesc ;

    // -------------------------------------------
    // Kalman Filter
    // -------------------------------------------

    int Pheight;
    int state_size;  int meas_size;
    Mat xhat,xhatminus;
    Mat A,AT;
    Mat P,Pminus;
    Mat Q,R;
    Mat z,Yk;
    Mat Snew,Knew;
    Mat Hk,zhat;
    Mat HP,HT;
    Mat S_Inv,HS;
    Mat I,KH;
    Mat IKH;
    double t ;//0.5;
    double k_kf;

    // intialising all nodes of a tree in an array
    Tree* nodeArray[500];


    // -------------------------------------------
    // Constructor
    // -------------------------------------------

    /*!
      @brief Default constructor for initializing the human tracker window.
      */
    Tracker()
    {
        writer.open("trackVideo.avi",CV_FOURCC('D','I','V','3'),30,Size(640,480),true);

        treecheckFile.open("treeCheckFile.txt");

        ffillMode = 1;
        loDiff = 20, upDiff = 20;

        selectTemplate = false;
        goodTemplateSearch = false;

        for(int i=0;i<100;i++)
            nodeArray[i] = newTreeNode(i);

        scale = 1.0;

        humanTemplates  = new HumanTemplate[100];

        areaVector.push_back(HEAD_AREA);
        areaVector.push_back(TORSO_AREA);
        areaVector.push_back(LEG_AREA);

        mySURF.extended = 0;
        mySURF.hessianThreshold = 10;
        descriptorMatcher = DescriptorMatcher::create( "FlannBased" );
        mactherFilterType = getMatcherFilterType( "CrossCheckFilter" );


        // -------------------------------------------
        // Inputs for Tracking
        // -------------------------------------------


        if(VIDEO==1)
        {
            cap.open("d.avi");
            if( !cap.isOpened() )
            {
                cout << "Could not initialize capturing...\n";
                //            exit_flag = true;
                exit(-1);
            }
        }
        else if(VIDEO==2)
        {
            firstFrame = imread("./dataset/seq03-img-right/image_00000100_1.png");

            if(firstFrame.empty())
            {
                cout << "empty frame read " << endl;
                exit(-1);
            }
        }
        else if(VIDEO==3)
        {
            firstFrame = imread("./dataset/seq00-img-left/image_00000180_0.png");

            if(firstFrame.empty())
            {
                cout << "empty frame read " << endl;
                exit(-1);
            }
        }
        else if(VIDEO==4)
        {
            firstFrame = imread("./dataset/seq05-img-left/image_00000100_0.png");

            if(firstFrame.empty())
            {
                cout << "empty frame read " << endl;
                exit(-1);
            }
        }
        else
        {
            cout << "No video selected" << endl;
            exit(-1);
        }



        if(VIDEO==1)
        {
            for(int i=0;i<40;i++)
                cap >> firstFrame;
        }

        imgsrc = firstFrame.clone();
        firstImg = firstFrame.clone();
        prevImg = firstFrame.clone();

#if(GMM)

        // -------------------------------------------
        // Mouse based ROI (polypoints) selection
        // -------------------------------------------

        Mat temp = firstFrame.clone();
        cvNamedWindow("imageInput");
        setMouseCallback("imageInput", mouseCB_polypoints);
        while(!pointSelectionDone)
        {
            firstFrame.copyTo(temp);

            for(unsigned int k=0; k<bodyPointsSelected.size(); k++)
                cv::circle(temp,bodyPointsSelected[k],2,Scalar(0,255,0),1,CV_AA);
            cv::imshow("imageInput",temp) ;

            char c = (char)waitKey(10);
            if( c == '\x1b' ) // esc
            {
                cout << "Exiting while region selection ..." << endl;
                destroyAllWindows();
                //                exit_flag = true;
                exit(-1);
            }
        }

        destroyWindow("imageInput");

        // hard coding the storage of some points, so that selection is not made each time

        //seq03-img-right/image_00000100_1.png
        if(VIDEO==2)
        {
            bodyPointsSelected.clear();
            bodyPointsSelected.push_back(Point2f(292,175));
            bodyPointsSelected.push_back(Point2f(289,186));
            bodyPointsSelected.push_back(Point2f(278,195));
            bodyPointsSelected.push_back(Point2f(269,213));
            bodyPointsSelected.push_back(Point2f(268,230));
            bodyPointsSelected.push_back(Point2f(275,234));
            bodyPointsSelected.push_back(Point2f(280,257));
            bodyPointsSelected.push_back(Point2f(282,286));
            bodyPointsSelected.push_back(Point2f(292,296));
            bodyPointsSelected.push_back(Point2f(292,286));
            bodyPointsSelected.push_back(Point2f(302,266));
            bodyPointsSelected.push_back(Point2f(303,254));
            bodyPointsSelected.push_back(Point2f(305,240));
            bodyPointsSelected.push_back(Point2f(306,221));
            bodyPointsSelected.push_back(Point2f(309,201));
            bodyPointsSelected.push_back(Point2f(304,193));
            bodyPointsSelected.push_back(Point2f(297,186));
            bodyPointsSelected.push_back(Point2f(298,175));
        }
        //         d.avi
        if(VIDEO==1)
        {
            bodyPointsSelected.clear();
            bodyPointsSelected.push_back(Point2f(274,16));
            bodyPointsSelected.push_back(Point2f(266,40));
            bodyPointsSelected.push_back(Point2f(271,58));
            bodyPointsSelected.push_back(Point2f(271,76));
            bodyPointsSelected.push_back(Point2f(258,79));
            bodyPointsSelected.push_back(Point2f(244,103));
            bodyPointsSelected.push_back(Point2f(241,146));
            bodyPointsSelected.push_back(Point2f(247,197));
            bodyPointsSelected.push_back(Point2f(257,223));
            bodyPointsSelected.push_back(Point2f(256,259));
            bodyPointsSelected.push_back(Point2f(270,331));
            bodyPointsSelected.push_back(Point2f(277,444));
            bodyPointsSelected.push_back(Point2f(284,459));
            bodyPointsSelected.push_back(Point2f(294,445));
            bodyPointsSelected.push_back(Point2f(310,431));
            bodyPointsSelected.push_back(Point2f(315,414));
            bodyPointsSelected.push_back(Point2f(307,411));
            bodyPointsSelected.push_back(Point2f(308,319));
            bodyPointsSelected.push_back(Point2f(309,291));
            bodyPointsSelected.push_back(Point2f(320,223));
            bodyPointsSelected.push_back(Point2f(317,153));
            bodyPointsSelected.push_back(Point2f(332,141));
            bodyPointsSelected.push_back(Point2f(322,91));
            bodyPointsSelected.push_back(Point2f(298,76));
            bodyPointsSelected.push_back(Point2f(295,57));
            bodyPointsSelected.push_back(Point2f(296,27));
            bodyPointsSelected.push_back(Point2f(285,15));
        }

        //        old lady
        if(VIDEO==4)
        {
            bodyPointsSelected.clear();
            bodyPointsSelected.push_back(Point2f(59,147));
            bodyPointsSelected.push_back(Point2f(52,153));
            bodyPointsSelected.push_back(Point2f(46,160));
            bodyPointsSelected.push_back(Point2f(45,173));
            bodyPointsSelected.push_back(Point2f(37,179));
            bodyPointsSelected.push_back(Point2f(32,184));
            bodyPointsSelected.push_back(Point2f(18,233));
            bodyPointsSelected.push_back(Point2f(22,251));
            bodyPointsSelected.push_back(Point2f(27,270));
            bodyPointsSelected.push_back(Point2f(22,300));
            bodyPointsSelected.push_back(Point2f(33,302));
            bodyPointsSelected.push_back(Point2f(38,333));
            bodyPointsSelected.push_back(Point2f(67,330));
            bodyPointsSelected.push_back(Point2f(68,301));
            bodyPointsSelected.push_back(Point2f(82,290));
            bodyPointsSelected.push_back(Point2f(82,257));
            bodyPointsSelected.push_back(Point2f(87,236));
            bodyPointsSelected.push_back(Point2f(84,207));
            bodyPointsSelected.push_back(Point2f(72,183));
            bodyPointsSelected.push_back(Point2f(74,160));
            bodyPointsSelected.push_back(Point2f(66,154));
        }
        // seq00 img left
        if(VIDEO==3)
        {
            bodyPointsSelected.clear();
            bodyPointsSelected.push_back(Point2f(365,190));
            bodyPointsSelected.push_back(Point2f(361,193));
            bodyPointsSelected.push_back(Point2f(361,202));
            bodyPointsSelected.push_back(Point2f(354,209));
            bodyPointsSelected.push_back(Point2f(350,217));
            bodyPointsSelected.push_back(Point2f(353,230));
            bodyPointsSelected.push_back(Point2f(355,247));
            bodyPointsSelected.push_back(Point2f(358,261));
            bodyPointsSelected.push_back(Point2f(362,277));
            bodyPointsSelected.push_back(Point2f(364,301));
            bodyPointsSelected.push_back(Point2f(369,312));
            bodyPointsSelected.push_back(Point2f(386,310));
            bodyPointsSelected.push_back(Point2f(386,293));
            bodyPointsSelected.push_back(Point2f(384,276));
            bodyPointsSelected.push_back(Point2f(382,253));
            bodyPointsSelected.push_back(Point2f(386,247));
            bodyPointsSelected.push_back(Point2f(398,240));
            bodyPointsSelected.push_back(Point2f(394,215));
            bodyPointsSelected.push_back(Point2f(378,207));
            bodyPointsSelected.push_back(Point2f(372,193));
        }
        box = boundingRect(bodyPointsSelected);
#else
        // ------------------------------------ Mouse based ROI (rectangle) selection ------------------------------//
        Mat temp = firstFrame.clone();
        cvNamedWindow("imageInput");
        setMouseCallback("imageInput", my_mouse_callback);
        while(!rect_drawn)
        {
            if(drawing_box)
            {
                firstFrame.copyTo(temp);

                rectangle(temp, box_selection, Scalar(0,0,255), 2);
            }
            cv::imshow("imageInput",temp) ;

            char c = (char)waitKey(10);
            if( c == '\x1b' ) // esc
            {
                cout << "Exiting while region selection ..." << endl;
                destroyAllWindows();
                exit(-1);
                //                exit_flag = true;
            }
        }

        destroyWindow("imageInput");

        box = box_selection;
#endif

        if(VERBOSE)
            cout << "box.x="   << box.x << "; \t box.y =" << box.y << ";\t box.width=" << box.width << "; \t box.height=" << box.height  << ";" << endl;


        // Dividing boxes in 3 parts(head,torso,leg)

        AR = float(box.width) / float(box.height);
        boxOrg = box; // storing the initial selected Box, as "box" variable changes in consecutive matching
        B1.x = boxOrg.x; B1.y = boxOrg.y; B1.width= boxOrg.width; B1.height =  int(0.15*boxOrg.height);
        B2.x = boxOrg.x; B2.y = boxOrg.y + int(0.15*boxOrg.height); B2.width= boxOrg.width; B2.height =  int(0.4*boxOrg.height);
        B3.x = boxOrg.x; B3.y = boxOrg.y + int(0.55*boxOrg.height); B3.width= boxOrg.width; B3.height =  int(0.45*boxOrg.height);

        Point2f center;
        center.x = (box.x+box.width/2); center.y= box.y +box.height/2;
        centerSurf.x = center.x; centerSurf.y = center.y;
        Size2f size;
        size.width= box.width/2; size.height=box.height/2;
        vector<Point> pnt;
        ellipse2Poly(center,size,0,0,360,12,pnt);
        Point pointArr[pnt.size()];
        for (unsigned int i=0; i< pnt.size(); i++)
            pointArr[i] = pnt[i];
        const Point* pointsArray[1] = {pointArr};
        int nCurvePts[1] = { pnt.size() };
        polylines(imgsrc, pointsArray, nCurvePts, 1, 2, Scalar(0,255,0), 2);

        //-----------------------------------------------
        // Find SURF features from the source image
        Mat img1ROI;
        vector<KeyPoint> tempkey;
        vector<Point2f> tempPoints;
        Mat tempDesc;
        vector<KeyPoint> myKeypoint;
        Mat myDescriptor;
        img1ROI = firstImg(boxOrg).clone();    mySURF.detect(img1ROI, tempkey);    KeyPoint::convert(tempkey, tempPoints);
        mySURF.compute(img1ROI, tempkey, tempDesc);

        // K-means clustering of all the descriptors to gert bins for histogram matching
        Mat labelsOrg;
        cv::kmeans(tempDesc,5,labelsOrg,TermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,50,1.0),1,KMEANS_RANDOM_CENTERS,clustersOrg);

        shiftPoints(tempPoints, boxOrg);
        bool flag = false;
        int torso_thresh=0,keysize=0;
        for(unsigned int i=0;i<tempPoints.size();i++)
        {
            flag = cv::pointPolygonTest(pnt, tempPoints[i], false );
            if(flag==1)
            {
                keysize++;
                myKeypoint.push_back(tempkey[i]);
                myDescriptor.push_back(tempDesc.row(i));
                if(B2.contains(tempPoints[i]))
                    torso_thresh++;
            }
        }


        rectangle(imgsrc,B1,Scalar(0,0,255),2,8,0);
        rectangle(imgsrc,B2,Scalar(0,255,0),2,8,0);
        rectangle(imgsrc,B3,Scalar(255,0,0),2,8,0);

        if(RESULTS_IMAGES)
            imshow("imgSrc",imgsrc);

        //+===========================================================================================
        // Color based tracking section

        Mat imgColor = firstImg(box).clone();
        Mat imgShowColor = imgColor.clone();
        int K=NO_OF_COLOR_CLUSTERS;
        Mat imglabels;
        cvtColor(imgColor, imgColor, CV_BGR2HSV);

        Mat samples;
        for(int j=0; j<imgColor.cols; j++)
        {
            for(int k=0; k<imgColor.rows; k++)
            {
                Mat temp(1,2,CV_32FC1);
                temp.at<float>(0,0) = float(imgColor.at<Vec3b>(k,j)[0]);
                temp.at<float>(0,1) = float(imgColor.at<Vec3b>(k,j)[1]);

                samples.push_back(temp.row(0));
            }
        }
#if(GMM)

        // -------------------------------------------
        // Segmentation of body parts pixels
        // -------------------------------------------

        Mat samples1, samples2, samples3, samples4, segmentImg = firstImg.clone(),samplesTest;
        cvtColor(segmentImg,segmentImg,CV_BGR2HSV);
        for(int j=0; j<segmentImg.cols; j++)
        {
            for(int k=0; k<segmentImg.rows; k++)
            {
                Mat temp(1,3,CV_64FC1);
                temp.at<double>(0,0) = 100*double(segmentImg.at<Vec3b>(k,j)[0])/180.0;
                temp.at<double>(0,1) = 100*double(segmentImg.at<Vec3b>(k,j)[1])/255.0;
                temp.at<double>(0,2) = 100*double(segmentImg.at<Vec3b>(k,j)[2])/255.0;
                //                temp.at<double>(0,2) = 0.1*100*double(ordinate)/boxOrg.height;

                Point2f thisPoint(j,k);
                if(boxOrg.contains(thisPoint))
                    samplesTest.push_back(temp.row(0));

                int check = pointPolygonTest(bodyPointsSelected,thisPoint,false);
                if(check == 1)
                {
                    if(B1.contains(thisPoint))
                    {
                        samples1.push_back(temp.row(0));
                        segmentImg.at<Vec3b>(k,j) = Vec3b(0,0,255);
                    }
                    else if(B2.contains(thisPoint))
                    {
                        samples2.push_back(temp.row(0));
                        segmentImg.at<Vec3b>(k,j) = Vec3b(0,255,0);
                    }
                    else if(B3.contains(thisPoint))
                    {
                        samples3.push_back(temp.row(0));
                        segmentImg.at<Vec3b>(k,j) = Vec3b(255,0,0);
                    }
                }
                else
                {
                    samples4.push_back(temp.row(0));
                }
            }
        }

        if(RESULTS_IMAGES)
            imshow("segmentImg",segmentImg);

        emModel1 = EM(1, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
        emModel2 = EM(1, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
        emModel3 = EM(1, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
        emModel4 = EM(5, EM::COV_MAT_DIAGONAL, TermCriteria(TermCriteria::COUNT+TermCriteria::EPS, 300, 0.1));
        trainGMM(emModel1,samples1,1);
        trainGMM(emModel2,samples2,1);
        trainGMM(emModel3,samples3,1);
        trainGMM(emModel4,samples4,5);

        Mat labelsTest;
        segmentBodyParts(emModel1,emModel2,emModel3,emModel4,samplesTest,labelsTest,GMM_NO_HEAD);

        for(int k=0; k<3; k++)
        {
            colors.push_back(Vec3b(0,0,0));
            colors[k][0] = 255 * ( rand() / (double)RAND_MAX );
            colors[k][1] = 255 * ( rand() / (double)RAND_MAX );
            colors[k][2] = 255 * ( rand() / (double)RAND_MAX );
        }

        Mat testClustering = firstImg(boxOrg).clone();
        int counter1=0;
        for(int j=0; j<testClustering.cols; j++)
        {
            for(int i=0; i<testClustering.rows; i++)
            {
                if(labelsTest.at<int>(counter1)!=-1)
                    testClustering.at<Vec3b>(i,j) = colors[labelsTest.at<int>(counter1)];
                else
                    testClustering.at<Vec3b>(i,j) = Vec3b(0,0,0);

                counter1++;
            }
        }

        dilate(testClustering,testClustering,Mat());
        if(RESULTS_IMAGES)
            imshow("testClustering",testClustering);

#else
        kmeans(samples,K,imglabels,TermCriteria( CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 300, 0.1 ), 1,KMEANS_RANDOM_CENTERS,imgMeans);

        for(int i=0;i<imgMeans.rows;i++)
        {
            Vec3b a = Vec3b(0,0,0);
            colors.push_back(a);
        }

        for(int k=0; k<imgMeans.rows; k++)
        {
            colors[k][0] = 255 * ( rand() / (double)RAND_MAX );
            colors[k][1] = 255 * ( rand() / (double)RAND_MAX );
            colors[k][2] = 255 * ( rand() / (double)RAND_MAX );
        }

        int counter=0;

        for(int j=0; j<imgColor.cols; j++)
        {
            for(int i=0; i<imgColor.rows; i++)
            {
                imgShowColor.at<Vec3b>(i,j) = colors[imglabels.at<int>(counter)];
                counter++;
            }
        }

        if(RESULTS_IMAGES)
            imshow("imgShowColor",imgShowColor);
#endif
        Mat temp2 = testClustering.clone();
        cvNamedWindow("imgShowColor");
        setMouseCallback("imgShowColor", mouseCB_selectParts);
        while(partSelected<3)
        {
            cv::imshow("imgShowColor",temp2) ;

            char c = (char)waitKey(10);
            if( c == '\x1b' ) // esc
            {
                cout << "Exiting while parts selection ..." << endl;
                destroyAllWindows();
                exit(-1);
            }
        }
        destroyAllWindows();

#if(GMM)
        extractPoints.push_back(0);
        extractPoints.push_back(1);
#if(!GMM_NO_HEAD)
        extractPoints.push_back(2);
#endif
        adjMat = Mat_<int>(extractPoints.size(),extractPoints.size(),-1);
        for(unsigned int k=0; k<extractPoints.size(); k++)
        {
            adjMat(k,k+1) = 1;
        }

#else
        for(int k=0; k<partPoints.size(); k++)
        {
            if(partPoints[k] == Point2f(0,0))
                continue;
            int index = imgColor.rows * partPoints[k].x + partPoints[k].y;
            int label = imglabels.at<int>(index);
            extractPoints.push_back(label);
            bodyClusters.push_back(imgMeans.row(label));
        }
        adjMat = Mat_<int>(extractPoints.size(),extractPoints.size(),-1);
        for(int k=0; k<extractPoints.size()-1; k++)
        {
            adjMat(k,k+1) = 1;
        }

#endif


        if(RESULTS_IMAGES)
        {
            destroyWindow("imgShowColor");
            destroyWindow("testClustering");
            destroyWindow("segmentImg");
        }


        //============================ Kalman filter ====================
        state_size = 4;
        meas_size = 2;
        xhat= Mat(state_size,1,CV_32F,0.0);                           xhatminus = Mat(state_size,1,CV_32F,0.0);
        A = Mat(state_size,state_size,CV_32F,0.0);                 AT = Mat(state_size,state_size,CV_32F,0.0);
        P = Mat(state_size, state_size, CV_32F,0.0);                   Pminus = Mat(state_size, state_size, CV_32F,0.0);
        Q = Mat (state_size, state_size, CV_32F,0.0);                  R = Mat(meas_size, meas_size, CV_32F,0.0);
        z = Mat(meas_size,1,CV_32F,0.0);                               Yk = Mat(meas_size,1,CV_32F,0.0);
        Snew = Mat(meas_size, meas_size, CV_32F,0.0);                     Knew = Mat(state_size, meas_size, CV_32F,0.0);
        Hk = Mat(meas_size, state_size,CV_32F,0.0);                zhat =  Mat(meas_size,1,CV_32F,0.0);
        HP = Mat(meas_size,state_size,CV_32F,0.0);                     HT= Mat( state_size,meas_size,CV_32F,0.0);
        S_Inv = Mat(meas_size, meas_size, CV_32F,0.0);                 HS = Mat(state_size, meas_size, CV_32F,0.0);
        I = Mat(state_size, state_size, CV_32F,0.0);                   KH = Mat(state_size, state_size, CV_32F,0.0);
        IKH = Mat(state_size, state_size, CV_32F,0.0);
        t=0.1 ;//0.5;
        eye(P,1.0);    eye(Pminus,1.0);    eye(R,0.1);
        k_kf=1;
        Hk.at<float>(0,0) = 1;    Hk.at<float>(1,1) = 1;

        finalWindow = boxOrg;

        KeyPoint::convert(myKeypoint,currentImgMatchPoint);
        currentMatchDesc = myDescriptor.clone();

        colorCurrentImg = firstImg.clone();
        currentImg = firstImg.clone();
        currentImgROI = currentImg(boxOrg).clone();

        colorCurrentImgROI = colorCurrentImg(boxOrg).clone();
        colorImg2 = colorCurrentImgROI.clone();
        surfShowImage = colorCurrentImgROI.clone();
#if(GMM)
        currentLabels = labelsTest.clone();
#else
        currentLabels = imglabels.clone();
#endif
        currentImgRect = boxOrg;
        searchWindow = boxOrg;
        boundingRectMRF = boxOrg;

        dataFile.open("dataFile.txt");
        descriptorFile.open("descriptorFile.txt");
        timerFile2.open("timerFile2.txt");
        timerFile2 << "#FrameNumber    templateSearchTime   initSurf  SURFmatch    predictor   imageProc   colorPart   final" << endl;
        humanDetectedMRF = false;
        exit_flag = false;
        surfMatched = false;
        avgDesc = 0;

        cvtColor(colorCurrentImgROI,hsvCurrentImgROI,CV_BGR2HSV);

        colorBasedSegmentation();



        //======================================================================================================================
    }
    ~Tracker()
    {
        writer.release();
    }

    /*!
      @brief This is the main function that calls other class functions for human tracking.
      */
    void trackHuman();

    /*!
      @brief Color based information is used to segment the human and perform graph matching.
      */
    void colorBasedSegmentation();

    /*!
      @brief Predicts the center of the human window using Kalman Filter.
      */
    void initPredictor();

    /*!
      @brief Updates the center of the human window if human has been detected.
      */
    void updatePredictor();

    /*!
      @brief This function searches for the template from the last node in the tree.
      It takes into account only its parent nodes and find out the node which has maximum percentage match.
      @return Returns the node number corresponding to the maximum matching.
    */
    int searchTemplateFromLastNode1(Mat queryDesc,vector<KeyPoint> queryKp);

    /*!
      @brief Attaches a new node in the tree containing the human templates.
    */
    void attachNodeInTree(int nodeNo);

    /*!
      @brief Initializes a new human template.
      */
    void addTemplate(Mat templateImg, vector<Point2f> pointsAfterColor, Mat descAftercolor, Mat combinedColorImage);


};

void Tracker::trackHuman()
{
#if(ROS)
        ros::Time t1 = ros::Time::now();
#endif
        frameNumber++;
    dataFile << frameNumber << "\t";
    timerFile2 << frameNumber << "\t";

    if(VERBOSE)
    {
        cout << "*******************************************************************************************************" << endl;
        cout << "FRAME NUMBER \t" << frameNumber << endl;
    }
    videoImage = currentImg.clone();

    if(kalmanCount>0)
    {
        initPredictor();
    }
    else
    {
        xhat.at<float>(0,0) = pCenter.x;
        xhat.at<float>(1,0) = pCenter.y;
        xhat.at<float>(2,0)=0.0;
        xhat.at<float>(3,0)=0.0;
        pBox=currentImgRect;
        goodTemplateSearch = true;
    }

    if(selectTemplate)
    {
        templateCount = totalTemplates-1;
        goodTemplateSearch = true;
    }
    if(!humanDetectedMRF && !surfMatched)
    {
        if(TREE)
        {
            templateCount = searchTemplateFromLastNode1(prevImgDesc,prevImgKeypoint);

            if(goodTemplateSearch)
            {
                treecheckFile << "TEMPLATE NUMBER \t" << templateCount << "\t totalTemplates \t" << totalTemplates << "\t";
                treecheckFile << humanDetectedMRF << "\t" << selectTemplate << "\t" << frameNumber << endl;

                if(templateCount != (totalTemplates-1))
                {
                    // Case when last template is not selected
                    char s1[200],s2[200],s3[200];
                    sprintf(s1,"./tree_use_cases/query%d.jpg",frameNumber);
                    sprintf(s2,"./tree_use_cases/result%d.jpg",frameNumber);
                    sprintf(s3,"./tree_use_cases/lastTemplate%d.jpg",frameNumber);

                    imwrite(s1,currentImg(finalWindow).clone());
                    imwrite(s2,humanTemplates[templateCount].humanImage);
                    imwrite(s3,humanTemplates[totalTemplates-1].humanImage);

                    //                    waitKey(0);
                }
            }


            if(VERBOSE)
                cout << "Finding the template \t" << goodTemplateSearch <<  endl;
        }
        else
        {
            templateCount = totalTemplates-1;
            goodTemplateSearch = true;
        }
    }

#if(ROS)
    timerFile2 << (ros::Time::now()-t1).toSec() << "\t";
    ros::Time t0 = ros::Time::now();
#endif
    currentImgPoints.clear();
    currentImgKeypoint.clear();
    currentImgDesc.release();

    Mat imageForKeypoint = currentImg(finalWindow).clone();
    mySURF.detect(imageForKeypoint,currentImgKeypoint);
    mySURF.compute(imageForKeypoint,currentImgKeypoint,currentImgDesc);
    shiftKeyPoints(currentImgKeypoint,finalWindow);
    KeyPoint::convert(currentImgKeypoint,currentImgPoints);

    int currentDesc = currentImgDesc.rows ;

    avgDesc = ((frameNumber-1)* avgDesc + currentDesc)/(frameNumber);

    descriptorFile << frameNumber << "\t" << currentImgDesc.rows << "\t" << avgDesc << endl;

#if(ROS)
    timerFile2 << (ros::Time::now() - t0) << "\t";
#endif

    if(goodTemplateSearch && (humanDetectedMRF || surfMatched))
    {
#if(ROS)
        ros::Time t2 = ros::Time::now();
#endif
        // Do matching and localise the human region using SURF matching

        // Attributes for templates
        prevImgROI = humanTemplates[templateCount].humanImage.clone();
        vector<Point2f> tempPoints, templateMatchPoints;
        tempPoints = humanTemplates[templateCount].humanKeypoint;
        vector<KeyPoint> templateKp;
        KeyPoint::convert(tempPoints,templateKp);
        Mat templateDesc = humanTemplates[templateCount].humanDescriptors.clone();

        currentImgMatchPoint.clear();
        currentMatchDesc.release();

        Point2f lastCenterTemp = centerSurf;
        centerSurf.x = humanTemplates[templateCount].humanBox.width/2;
        centerSurf.y = humanTemplates[templateCount].humanBox.height/2;

        // Finding Surf matching of the template with current Frame
        int numMatch=0;
        double match = surfDesc_Matching2(prevImgROI,templateDesc,templateKp,currentImg,currentImgDesc,currentImgKeypoint,
                                          templateMatchPoints,currentImgMatchPoint,currentMatchDesc,finalWindow,centerSurf,
                                          numMatch);

        if(match < 20 || norm(lastCenterTemp-centerSurf) > 50 || numMatch < MIN_MATCH_COUNT)
        {
            surfMatched = false;
            selectTemplate = false;
        }
        else if(match > 20)
        {
            humanDetectedMRF = false;
            surfMatched = true;

        }

        dataFile << currentMatchDesc.rows << "\t";
        if(VERBOSE)
            cout << "MATCHES \t" << match << endl;

        if(surfMatched)
        {
            // Scaling the currentBox found using surf matching with respect to template aspect ratio
            AR = (double)(humanTemplates[templateCount].humanBox.width)/(double)(humanTemplates[templateCount].humanBox.height);
            scale = humanTemplates[templateCount].scaleFactor;
            scalebox(currentImgRect,humanTemplates[templateCount].humanBox,templateMatchPoints,currentImgMatchPoint,
                     scale,AR,SCALE_ALLOWED,centerSurf,boxOrg);


            float scaleRatio = float(CHANGE_IN_SCALING)/100.0;
            if(scale > 1+scaleRatio || scale < 1-scaleRatio)
            {
                selectTemplate = true;
                float factor = EXTRA_SCALING_FACTOR;
                searchWindow = currentImgRect;
                extraScaleRectangle(currentImgRect,factor);
            }
            else
                selectTemplate = false;
        }
        else
        {
            currentImgRect = pBox;
            centerSurf = Point2f(pBox.x+pBox.width/2,pBox.y+pBox.height/2);

            // Extra scaling the box so that we can get a proper human present in the frame
            float factor = EXTRA_SCALING_FACTOR;
            searchWindow = currentImgRect;
            extraScaleRectangle(currentImgRect,factor);

            checkBoundaryRect(currentImgRect);
        }

#if(ROS)
        timerFile2 << (ros::Time::now()-t2).toSec() << "\t" << 0 << "\t";
#endif
    }
    // Do color segmentation for human so that we can select a new human template
    else
    {
#if(ROS)
        ros::Time t2 = ros::Time::now();
#endif
        currentImgRect = pBox;

        float factor = EXTRA_SCALING_FACTOR;
        searchWindow = currentImgRect;
        extraScaleRectangle(currentImgRect,factor);

        checkBoundaryRect(currentImgRect);

        surfMatched = false;

        dataFile << 0 << "\t";
#if(ROS)
        timerFile2 << 0 << "\t" << (ros::Time::now()-t2).toSec() << "\t";
#endif
    }

#if(ROS)
    ros::Time t3 = ros::Time::now();
#endif
    dataFile << currentImgRect.area() << endl;

    // ROI of the current Image
    currentImgROI = currentImg(currentImgRect).clone();

    // Additional colorImage ROI
    colorCurrentImgROI = currentImgROI.clone();

    // For processing color information in colorSegmentation function
    colorImg2 = currentImgROI.clone();

    // Converting color images in RGB space to HSV color space
    cvtColor(colorCurrentImgROI,hsvCurrentImgROI,CV_BGR2HSV);

    //    cout << "1" << ros::Time::now() << endl;
    currentsamples.release();
    // Function to do kmeans clustering , here we are finding the respective bins of the color pixel values of currentImage

    if(!surfMatched || selectTemplate)
    {
#if(GMM)
        for(int j=0; j<hsvCurrentImgROI.cols; j++)
        {
            for(int k=0; k<hsvCurrentImgROI.rows; k++)
            {
                Mat temp1(1,3,CV_64FC1);
                temp1.at<double>(0,0) = 100*double(hsvCurrentImgROI.at<Vec3b>(k,j)[0])/180.0;
                temp1.at<double>(0,1) = 100*double(hsvCurrentImgROI.at<Vec3b>(k,j)[1])/255.0;
                temp1.at<double>(0,2) = 100*double(hsvCurrentImgROI.at<Vec3b>(k,j)[2])/255.0;

                currentsamples.push_back(temp1.row(0));
            }
        }
#else
        for(int j=0; j<hsvCurrentImgROI.cols; j++)
        {
            for(int k=0; k<hsvCurrentImgROI.rows; k++)
            {
                Mat temp1(1,2,CV_32FC1);
                temp1.at<float>(0,0) = float(hsvCurrentImgROI.at<Vec3b>(k,j)[0]);
                temp1.at<float>(0,1) = float(hsvCurrentImgROI.at<Vec3b>(k,j)[1]);

                currentsamples.push_back(temp1.row(0));
            }
        }
#endif
        int nCluster = NO_OF_COLOR_CLUSTERS;
        // Labelling the sample pixels with different body part labels
#if(GMM)
        segmentBodyParts(emModel1,emModel2,emModel3,emModel4,currentsamples,currentLabels,GMM_NO_HEAD);
#else
        searchBin(currentsamples,imgMeans,currentLabels,nCluster,extractPoints);
#endif

#if(ROS)
        ros::Time t4 = ros::Time::now();
        timerFile2 << (t4-t3).toSec() << "\t";
#endif

        colorBasedSegmentation();
#if(ROS)
        timerFile2 << (ros::Time::now() - t4).toSec() << "\t";
#endif
    }
    else
    {
#if(ROS)
        timerFile2 << 0 << "\t" << 0 << "\t";
#endif
    }

#if(ROS)
    ros::Time t5 = ros::Time::now();
#endif
    if(humanDetectedMRF || surfMatched)
    {
        finalWindow = currentImgRect;

        pCenter.x = int(currentImgRect.x + currentImgRect.width/2);
        pCenter.y = int(currentImgRect.y + currentImgRect.height/2);

        rectangle(videoImage,finalWindow,Scalar(0,0,255),2,8,0);

        updatePredictor();
    }
    else
    {
        if(VERBOSE)
            cout << "DO NOT UPDATE FILTER" << endl;
        finalWindow = pBox;
        xhat = xhatminus;
        centerSurf = Point2f(pBox.x+pBox.width/2,pBox.y+pBox.height/2);
    }


    prevImgKeypoint = currentImgKeypoint;
    prevImgDesc = currentImgDesc.clone();
    char s[200];
    sprintf(s,"%d",frameNumber);
    putText(videoImage,s,Point(500,50),2,2.0,Scalar(0,0,255),2,8);
    rectangle(videoImage,pBox,Scalar(255,255,255),2,8,0);

    if(!NO_IMGPROC)
    {
//        if(surfMatched)
//        {
//            sprintf(s,"SURF Matched");
//            putText(videoImage,s,Point(30,30),2,1,Scalar(0,0,255),1,CV_AA);
//        }
//        if(humanDetectedMRF)
//        {
//            sprintf(s,"Color Matched");
//            putText(videoImage,s,Point(30,80),2,1,Scalar(0,0,255),1,CV_AA);
//        }

        imshow("final",videoImage);

    }
    writer.write(videoImage);
    kalmanCount++;

    char exitChar = waitKey(WAITKEY);
    if(exitChar == '\x1b')
    {
        writer.release();
        exit(-1);
    }

#if(ROS)
    ros::Time t6 = ros::Time::now();
    timerFile2 << (t6-t5).toSec() << endl;
#endif
    if(exitChar == 'p' || exitChar == 'P')
    {
        if(WAITKEY == 0)
            WAITKEY = 1;
        else
            WAITKEY = 0;

    }
}

void Tracker::colorBasedSegmentation()
{

    Mat combinedColorImage = Mat(currentImgROI.rows,currentImgROI.cols,CV_8UC3,Scalar(0,0,0));
    Mat rawColorImage = Mat(currentImgROI.rows,currentImgROI.cols,CV_8UC3,Scalar(0,0,0));

    if(!startFlag)
    {
        currentImgMatchPoint.clear();
        currentMatchDesc.release();
        currentImgMatchPoint = currentImgPoints;
        currentMatchDesc = currentImgDesc.clone();

    }

    vector< vector<Point2f> > patchCenters(extractPoints.size());   // stores the centers of bounding rectangles of connected components
    vector< vector<Rect> > patchROIs(extractPoints.size());     // stores the bounding rectangle ROIs of connected components
    Mat colorImg3;
    // Iterate over all three body parts in order of head, torso and leg
    for(unsigned int iter=0;iter<extractPoints.size();iter++)
    {
        colorImg3 = colorImg2.clone();
        int count = 0;
        for(int j=0;j<colorCurrentImgROI.cols;j++)
        {
            for(int i=0;i<colorCurrentImgROI.rows;i++)
            {
                if(currentLabels.at<int>(count) == extractPoints[iter])
                {
                    colorImg2.at<Vec3b>(i,j) = Vec3b(255,255,255);
                    colorImg3.at<Vec3b>(i,j) = Vec3b(colors[extractPoints[iter]]);
                    rawColorImage.at<Vec3b>(i,j) = Vec3b(colors[extractPoints[iter]]);

                    // Discarding pixels of head below a certain height
                    if(extractPoints.size() == 3 && iter == 0)
                    {
                        if(i > 0.3*currentImgROI.rows)
                        {
                            colorImg2.at<Vec3b>(i,j) = Vec3b(0,0,0);
                            colorImg3.at<Vec3b>(i,j) = Vec3b(0,0,0);
                        }
                    }
                }
                else
                {
                    colorImg2.at<Vec3b>(i,j) = Vec3b(0,0,0);
                    colorImg3.at<Vec3b>(i,j) = Vec3b(0,0,0);
                }
                count++;
            }

        }

        dilate(colorImg2,colorImg2,Mat());
        dilate(colorImg3,colorImg3,Mat());

        // floodFill to connect only connected component
        Rect patchRect;
        Scalar newVal = Scalar(0,0,0);

        Vector<Point2f> seedPoints;

        int lo = ffillMode == 0 ? 0 : loDiff;
        int up = ffillMode == 0 ? 0 : upDiff;

        // connected component analysis on each pixel, and returned area thresholded
        for(int j=0;j<colorImg2.cols;j++)
        {
            for(int i=0;i<colorImg2.rows;i++)
            {
                Point2f seed;
                seed.x = j;
                seed.y = i;

                if(colorImg2.at<Vec3b>(i,j) == Vec3b(255,255,255))
                {
                    seed.x = j;
                    seed.y = i;
                    int area = cv::floodFill(colorImg2,seed,newVal,&patchRect,Scalar(lo,lo,lo),Scalar(up,up,up),4);

                    if(area < areaVector[iter])
                    {
                        seedPoints.push_back(seed);
                    }
                    else
                    {
                        Point2f center;
                        center.x = patchRect.x + patchRect.width/2;
                        center.y = patchRect.y + patchRect.height/2;
                        patchCenters[iter].push_back(center);
                        patchROIs[iter].push_back(patchRect);
                    }

                }
            }
        }

        // Repaint the connected components with newVal=Black color
        for(unsigned int i=0;i<seedPoints.size();i++)
        {
            int a = cv::floodFill(colorImg3,seedPoints[i],newVal,&patchRect,Scalar(lo,lo,lo),Scalar(up,up,up),4);
        }

        seedPoints.clear();


        for(int j=0;j<colorImg3.cols;j++)
        {
            for(int i=0;i<colorImg3.rows;i++)
            {
                if(colorImg3.at<Vec3b>(i,j) != Vec3b(0,0,0))
                {
                    combinedColorImage.at<Vec3b>(i,j) = colors[extractPoints[iter]];
                }
            }
        }


    }

    // AN extra image to view MRF selected patches
    Mat patchCentersImage = combinedColorImage.clone();

    // This part is used to gather torso patches together into one blob
#if(!GMM)
    vector<Point2f> patchCentersTorso;
    Point2f meanTorso(0,0);
    int torsoPtCounter=0;
    Point2f windowCenter(searchWindow.x+searchWindow.width/2,searchWindow.y+searchWindow.height/2);

    vector<Point2f> torsoPointsPrev = extractPoints.size() > 2 ? patchCenters[1] : patchCenters[0];
    // merge torso points based on distance from center of window
    for(int k=0; k<torsoPointsPrev.size(); k++)
    {
        Point2f patchCenterGlobal = torsoPointsPrev[k] + Point2f(currentImgRect.x,currentImgRect.y);
        if(abs(patchCenterGlobal.x-windowCenter.x) < searchWindow.width/2)
        {
            meanTorso += torsoPointsPrev[k];
            torsoPtCounter++;
            //            waitKey(0);
        }
        else
        {
            patchCentersTorso.push_back(torsoPointsPrev[k]);
        }
    }
    if(torsoPtCounter != 0)
    {
        meanTorso.x /= torsoPtCounter;
        meanTorso.y /= torsoPtCounter;
        patchCentersTorso.push_back(meanTorso);
    }

    torsoPointsPrev = patchCentersTorso;

    if(extractPoints.size()>2)
        patchCenters[1] = torsoPointsPrev;
    else
        patchCenters[0] = torsoPointsPrev;
#endif

    // store final selected patch centers
    vector<Point2f> finalPatches(extractPoints.size());
    // store indicies of selected patches from candidate patches
    vector<int> matchIndex(extractPoints.size());
    // flags for parts that have been detected
    vector<int> partsFound(extractPoints.size(),0);

    // Test if there are candidates available for a patch for applying MRF
    bool allPartsAvailable = true;
    for(unsigned int k=0; k<patchCenters.size(); k++)
    {
        if(patchCenters[k].size() == 0)
        {
            if(VERBOSE)
                cout << "\nOne of the parts has no candiates, so NO Human Detected " << endl;
            humanDetectedMRF = false;
            allPartsAvailable = false;
            break;
        }
    }

    float ARerror=100;

    // if candidates are available, apply MRF-MAP matching
    if(allPartsAvailable)
    {
        findMatch_MRF_MAP(adjMat,patchCenters,finalPatches,partsFound,matchIndex);

        // Test if all parts are found using MRF-MAP matching
        bool allPartsFound = true;
        for(unsigned int k=0; k<partsFound.size(); k++)
        {
            if(partsFound[k] == -1)
            {
                if(VERBOSE)
                    cout << "\nOne of the parts not detected, so NO Human Detected " << endl;
                humanDetectedMRF = false;
                allPartsFound = false;
                break;
            }
        }

        // if all parts are found using graph matching
        if(allPartsFound)
        {

#if(GMM_NO_HEAD)
            float torsoLeg = norm(finalPatches[0]-finalPatches[1]);

            float minHeight = 0.3*searchWindow.height;
            float maxHeight = 0.8*searchWindow.height;

            if(torsoLeg < maxHeight && torsoLeg > minHeight)
                humanDetectedMRF = true;
            else
            {
                humanDetectedMRF = false;
                if(VERBOSE)
                    cout << "Overall height doesnt match - Required range is " << minHeight << " to " << maxHeight
                         << " Found Value is " << torsoLeg << endl;
            }


            //            humanDetectedMRF = true;
#else
            // Check height constraint for the found human
            float headLeg = norm(finalPatches[0]-finalPatches[2]);

            // generate bounding rectangle for the selected patches
            vector<Point2f> roiTlBr;
            roiTlBr.push_back(patchROIs[0][matchIndex[0]].tl());// index 0 for torso
            roiTlBr.push_back(patchROIs[0][matchIndex[0]].br());
            roiTlBr.push_back(patchROIs[1][matchIndex[1]].tl());
            roiTlBr.push_back(patchROIs[1][matchIndex[1]].br());
            if(extractPoints.size() > 2)
            {
                roiTlBr.push_back(patchROIs[2][matchIndex[2]].tl());
                roiTlBr.push_back(patchROIs[2][matchIndex[2]].br());
            }

            boundingRectMRF = boundingRect(roiTlBr);
            // -1 because rect.br() point is not supposed to be added from the rectangles
            boundingRectMRF.width -= 1;
            boundingRectMRF.height -= 1;

            float ARnow = (float)boundingRectMRF.width/(float)boundingRectMRF.height;
            float ARorg = (float)boxOrg.width/(float)boxOrg.height;

            ARerror = 100*(abs(ARnow-ARorg))/(ARorg);

            float minHeight = 0.5*searchWindow.height;
            float maxHeight = 0.7*searchWindow.height;

            if(headLeg < maxHeight && headLeg > minHeight)
                humanDetectedMRF = true;
            else
            {
                humanDetectedMRF = false;
                if(VERBOSE)
                    cout << "Overall height doesnt match - Required range is " << minHeight << " to " << maxHeight
                         << " Found Value is " << headLeg << endl;
            }

#endif
            // if the constraint is satisfied proceed further
            if(humanDetectedMRF)
            {

                line(patchCentersImage,finalPatches[0], finalPatches[1], Scalar(255,255,255), 2, CV_AA);
                if(extractPoints.size()>2)
                    line(patchCentersImage,finalPatches[1], finalPatches[2], Scalar(255,255,255), 2, CV_AA);

                if(VERBOSE)
                    cout << "\nAll parts detected, HUMAN DETECTED " << endl;

#if(GMM_NO_HEAD)
                Point2f centerMRF = 0.5*( patchROIs[0][matchIndex[0]].tl() + patchROIs[0][matchIndex[0]].br() );

                boundingRectMRF.x = centerMRF.x - searchWindow.width/2;
                boundingRectMRF.y = centerMRF.y - 0.35*searchWindow.height;
                boundingRectMRF.width = searchWindow.width;
                boundingRectMRF.height = searchWindow.height;
                checkBoundaryRect(boundingRectMRF,Size(patchCentersImage.cols,patchCentersImage.rows));

                float ARnow = (float)boundingRectMRF.width/(float)boundingRectMRF.height;
                float ARorg = (float)boxOrg.width/(float)boxOrg.height;

                ARerror = 100*(abs(ARnow-ARorg))/(ARorg);
#endif

                rectangle(patchCentersImage,boundingRectMRF,Scalar(255,0,255));
            }

        }

    }

    // Draw color patches on an image
    Scalar colorPatches[] = {Scalar(0,255,255),Scalar(0,255,0), Scalar(0,0,255)};

    for(unsigned int i=0; i<patchCenters.size(); i++)
        for(unsigned int j=0; j<patchCenters[i].size(); j++)
        {
            circle(patchCentersImage,patchCenters[i][j], 5-i, colorPatches[i], 3, CV_AA);
        }

    Mat colorMRFselectedImage  = Mat::zeros(combinedColorImage.rows,combinedColorImage.cols,CV_8UC3);

    // Erase all the patches which have not been selected by graph matching, and generate a new clean image
    for(unsigned int j=0; j<patchROIs.size(); j++)
        for(unsigned int k=0; k<patchROIs[j].size(); k++)
        {
            Rect roiTemp = patchROIs[j][k];
            if(matchIndex[j]==k)
            {
                combinedColorImage(roiTemp).copyTo(colorMRFselectedImage(roiTemp));
            }
            else
            {
                Mat erasePatch = Mat::zeros(roiTemp.height,roiTemp.width,CV_8UC3);
                erasePatch.copyTo(colorMRFselectedImage(roiTemp));
            }

        }


    if(!NO_IMGPROC)
        imshow("patches", patchCentersImage);

    colorImg2.release();
    colorImg3.release();



    if(RESULTS_IMAGES)
    {
        imshow("mrfSelected", colorMRFselectedImage);
        imshow("Mycombined ",combinedColorImage);
        imshow("rawcolor",rawColorImage);
    }

    // Finding the bounding rectangle of humanPoints defined through color
    Rect colorRect;
#if(!GMM_NO_HEAD)
    Rect tempRect = boundingRectMRF;//cv::boundingRect(fullHumanPoints);
    Point2f center;
    center.x = tempRect.x + tempRect.width/2;
    center.y = tempRect.y + tempRect.height/2;
    colorRect.width = searchWindow.width;
    colorRect.height = searchWindow.height;
    colorRect.x = center.x - colorRect.width/2;
    colorRect.y = center.y - colorRect.height/2;
    colorRect.y = boundingRectMRF.y;

    {
        if(humanDetectedMRF)
        {
            if(ARerror < 20)
            {
                colorRect = boundingRectMRF;
            }
        }
    }
#else
    colorRect = boundingRectMRF;
#endif

    checkBoundaryRect(colorRect);

    Rect globalColorRect;
    // Finding the ROI of the color Rectangle with respect to global origin of the image
    globalColorRect.x = colorRect.x + currentImgRect.x;
    globalColorRect.y = colorRect.y + currentImgRect.y;
    globalColorRect.width = colorRect.width;
    globalColorRect.height = colorRect.height;

    checkBoundaryRect(globalColorRect);

    if(!selectTemplate && !surfMatched)
    {
        currentImgRect = globalColorRect;
    }
    else
        currentImgRect = searchWindow;


    surfShowImage = currentImg(currentImgRect).clone();
    if(!startFlag)
        shiftPoints_G2L(currentImgMatchPoint,currentImgRect);

    vector<Point2f> pointsAfterColor;
    Mat descAftercolor;
    // Removing matching descriptors which do not belong to human on the basis of color information
    for(unsigned int i=0;i<currentImgMatchPoint.size();i++)
    {
        Point2i testPoint = Point2i(currentImgMatchPoint[i].x,currentImgMatchPoint[i].y);
        Vec3b testPointColor = colorMRFselectedImage.at<Vec3b>(testPoint.y,testPoint.x);

        if(testPointColor != Vec3b(0,0,0) && boundingRectMRF.contains(currentImgMatchPoint[i]) )
        {
            pointsAfterColor.push_back(currentImgMatchPoint[i]);
            descAftercolor.push_back(currentMatchDesc.row(i));
        }
    }


    Mat templateImg;

    if(humanDetectedMRF)
    {
        float scaleRatio = float(CHANGE_IN_SCALING)/100.0;
        if(scale < 1-scaleRatio || !goodTemplateSearch || scale > 1+scaleRatio)
            if(ARerror < 30)
            {
                //                cout << "Scaling \t" << frameNumber << "\t" << scale << endl;
                selectTemplate = true;
                appendMatrixHorz(surfShowImage,patchCentersImage(boundingRectMRF),templateImg);
            }
            else
            {
                selectTemplate = false;
            }

        updateFilter = true;

    }
    else
    {
        // Case when human is not present and it goes into occlusion case
        selectTemplate = false;
        updateFilter = false;
    }

    //    treecheckFile << selectTemplate << "\t" << match << "\t" << scale << "\t" << surfMatched << endl;

    if(startFlag)
    {

        // initial template

        //        humanTemplates[totalTemplates].hEllipse = headEllipse;
        //        humanTemplates[totalTemplates].lEllipse = legEllipse;
        //        humanTemplates[totalTemplates].tEllipse = torsoEllipse;
        humanTemplates[totalTemplates].humanKeypoint = pointsAfterColor;
        humanTemplates[totalTemplates].humanDescriptors = descAftercolor.clone();
        humanTemplates[totalTemplates].humanImage = currentImgROI.clone();
        humanTemplates[totalTemplates].humanBox = currentImgRect;
        //        humanTemplates[totalTemplates].h2t = norm(headEllipse.center-torsoEllipse.center);
        //        humanTemplates[totalTemplates].t2l = norm(torsoEllipse.center - legEllipse.center);
        //        humanTemplates[totalTemplates].humanStickImage = currentStickImage.clone();
        humanTemplates[totalTemplates].surfImage = surfShowImage.clone();
        humanTemplates[totalTemplates].colorHumanImage = combinedColorImage.clone();
        humanTemplates[totalTemplates].scaleFactor = scale;

        appendMatrixHorz(surfShowImage,patchCentersImage(boundingRectMRF),templateImg);
        Mat writeImage = templateImg.clone();
        //        appendMatrixHorz(writeImage,combinedColorImage);
        //        appendMatrixHorz(writeImage,currentStickImage);
        //        appendMatrixHorz(writeImage,surfShowImage);

        char as[200];
        sprintf(as,"./templates/%d.jpg",totalTemplates);

        imwrite(as,writeImage);

        pCenter.x = int(currentImgRect.x + currentImgRect.width/2);
        pCenter.y = int(currentImgRect.y + currentImgRect.height/2);

        if(RESULTS_IMAGES)
        {
            imshow("desc",surfShowImage);
        }
        totalTemplates++;

    }
    else if(selectTemplate)
    {
        //    else if(colorAspectRatio > 0.21 && colorAspectRatio < 0.26)

        // new template

        if(pointsAfterColor.size() > 0)
        {
            addTemplate(templateImg,pointsAfterColor,descAftercolor,combinedColorImage);
        }
    }

    colorCurrentImg.release();
    colorCurrentImgROI.release();    surfShowImage.release();
    currentImg.release();currentImgROI.release();    currentImgMatchPoint.clear();    currentMatchDesc.release();
    currentsamples.release();    currentLabels.release();     hsvCurrentImgROI.release();
    descAftercolor.release();

    //        waitKey(WAITKEY);
}

void Tracker::addTemplate(Mat templateImg, vector<Point2f> pointsAfterColor, Mat descAftercolor, Mat combinedColorImage)
{
    //    humanTemplates[totalTemplates].hEllipse = headEllipse;
    //    humanTemplates[totalTemplates].lEllipse = legEllipse;
    //    humanTemplates[totalTemplates].tEllipse = torsoEllipse;
    humanTemplates[totalTemplates].humanKeypoint = pointsAfterColor;
    humanTemplates[totalTemplates].humanDescriptors = descAftercolor.clone();
    humanTemplates[totalTemplates].humanImage = currentImgROI.clone();
    humanTemplates[totalTemplates].humanBox = currentImgRect;
    //    humanTemplates[totalTemplates].h2t = norm(headEllipse.center-torsoEllipse.center);
    //    humanTemplates[totalTemplates].t2l = norm(torsoEllipse.center - legEllipse.center);
    //        humanTemplates[totalTemplates].humanStickImage = currentStickImage.clone();
    humanTemplates[totalTemplates].surfImage = surfShowImage.clone();
    humanTemplates[totalTemplates].colorHumanImage = combinedColorImage.clone();
    humanTemplates[totalTemplates].scaleFactor = scale;


    Mat writeImage = templateImg.clone();

    char as[200];
    sprintf(as,"./templates/%d.jpg",totalTemplates);

    imwrite(as,writeImage);


    attachNodeInTree(totalTemplates);
    totalTemplates++;
}

void Tracker::initPredictor()
{

    eye(A,1.0);
    int t = 0;
    A.at<float>(0,2) = t;
    A.at<float>(1,3) = t;
    eye(Q,1.0);
    Q.at<float>(0,0) = t*t*t*t/4;
    Q.at<float>(1,1) = t*t*t*t/4;
    Q.at<float>(2,2) = t*t;
    Q.at<float>(3,3) = t*t;
    Q.at<float>(0,2) = t*t*t/2;
    Q.at<float>(1,3) = t*t*t/2;
    Q.at<float>(2,0) = t*t*t/2;
    Q.at<float>(3,1) = t*t*t/2;
    multiply(Q,k_kf);
    transpose(A,AT);
    xhatminus = A*xhat;
    Pminus = A*P*AT + Q;
    zhat = Hk*xhatminus;
    Pheight = finalWindow.height;
    pCp.x = zhat.at<float>(0,0);
    pCp.y = zhat.at<float>(1,0);
    pBox.height=Pheight;
    pBox.width = int(AR*Pheight);
    pBox.x = int(pCp.x-pBox.width/2);
    pBox.y = int(pCp.y-pBox.height/2);

    //    cout << "INSIDE DETAILS :" << endl;
    //    cout << "A init \t" << A << endl;
    //    cout << "Q init \t" << Q << endl;
    //    cout << "P init \t" << P << endl;
    //    cout << "Pminus in init \t" << Pminus << endl;
    //    cout << "width and height \t" << pBox.width << "\t" << pBox.height << endl;
    //    cout << "x and y locations \t" << pBox.x << "\t" << pBox.y << endl;
    //    cout << "pcP points \t" << pCp.x << "\t" << pCp.y << endl;
    //    cout << "Pheight \t" << Pheight << endl;
    //    cout << "Hk \t" << Hk << endl;
    //    cout << "xhatminus \t" << xhatminus << endl;
    //    cout << "A \t" << A << endl;
    //    cout << "xhat \t" << xhat << endl;


    checkBoundaryRect(pBox);

    return ;
}

void Tracker::updatePredictor()
{
    z.at<float>(0,0)= pCenter.x;            z.at<float>(1,0)= pCenter.y;
    Yk.at<float>(0,0) = (z.at<float>(0,0)-zhat.at<float>(0,0));
    Yk.at<float>(1,0) = (z.at<float>(1,0)-zhat.at<float>(1,0));
    if(VERBOSE)
        cout << "UPDATE " << endl;
    //    cout << "UPDATE DETILAS : \t" << endl;
    //    cout << "z \t" << z << endl;
    //    cout << "Yk \t" << Yk << endl;
    HP = Hk*Pminus;
    transpose(Hk, HT);
    Snew = HP*HT + R;
    S_Inv = Snew.inv(cv::DECOMP_LU);
    HS = HT*S_Inv;
    //    cout << "Pminus \t" << Pminus << endl;
    //    cout << "HS \t" << HS << endl;
    Knew = Pminus*HS;//*sf;
    xhat =  xhatminus + Knew * Yk ;
    //    cout << "Yk \t" << Yk << endl;
    //    cout << "knew \t" << Knew << endl;
    //    cout << "xhat \t" << xhat << endl;
    KH = Knew * Hk;
    IKH = I - KH;
    P= IKH * Pminus;
    //    cout << "KH \t" << KH << endl;
    //    cout << "I \t" << I << endl;
    //    cout << "IKH \t" << IKH << endl;
    //    cout << "P niche \t" << P << endl;
    return ;
}

int Tracker::searchTemplateFromLastNode1(Mat queryDesc,vector<KeyPoint> queryKp)
{
    int template_id = 0;
    int nodeNumber = totalTemplates-1;


    vector<double> matchVector, matchVectorHD;
    vector<int> indexVector;

    for(int i=0;i<8;i++)
    {
        vector<KeyPoint> tempKey;
        KeyPoint::convert(humanTemplates[nodeNumber].humanKeypoint,tempKey);
        Mat descTemplate = humanTemplates[nodeNumber].humanDescriptors;
        double matchVal, matching_percentage;
#if(HISTOGRAM)
        matchVal = matchHistogram(queryDesc,descTemplate,clustersOrg);
        matchVectorHD.push_back(matchVal);
#else
        matching_percentage = surfDesc_Matching1(queryDesc,queryKp,descTemplate,tempKey);
        //        double matching_percentage = surfDesc_Matching(imgNew,humanTemplates[nodeNumber].humanDescriptors,tempKey);
        matchVector.push_back(matching_percentage);
#endif

        indexVector.push_back(nodeNumber);


        if(nodeArray[nodeNumber]->parent != NULL)
        {
            nodeNumber = nodeArray[nodeNumber]->parent->data;
        }
        else
        {
            break;
        }
    }

    double max = 0.0;
    int maxIndex = 0;
    double min = 999;
    int minIndex = -1;
#if(!HISTOGRAM)
    for(int i=0;i<matchVector.size();i++)
    {
        if(matchVector[i] > max)
        {
            max = matchVector[i];
            maxIndex = indexVector[i];
        }
    }
#else
    for(unsigned int i=0; i<matchVectorHD.size(); i++)
    {
        if(matchVectorHD[i] < min)
        {
            min = matchVectorHD[i];
            minIndex = indexVector[i];
        }
    }
#endif

#if(!HISTOGRAM)
    if(max > MATCHING_THRESHOLD && humanTemplates[maxIndex].humanKeypoint.size() > 25)
        goodTemplateSearch = true;
    else
        goodTemplateSearch = false;

    template_id = maxIndex;

#else
    if(min*100 < HIST_MATCH_THRESH && humanTemplates[minIndex].humanKeypoint.size() > 25)
        goodTemplateSearch = true;
    else
        goodTemplateSearch = false;

    template_id = minIndex;

#endif



    return template_id;
}
// This function attaches each node one by node on the basis of matching threshold.
void Tracker::attachNodeInTree(int nodeNo)
{

    int init_id = nodeArray[0]->data;

    Mat query_descriptors = humanTemplates[nodeNo].humanDescriptors ;
    vector<KeyPoint> query_keypoints;
    KeyPoint::convert(humanTemplates[nodeNo].humanKeypoint,query_keypoints);

    if(query_keypoints.size() == 0)
    {
        return;
    }

    while(1)
    {
        vector<KeyPoint> tempK;
        KeyPoint::convert(humanTemplates[init_id].humanKeypoint,tempK);

        double matchVal, matching_percentage;
#if(HISTOGRAM)
        matchVal = matchHistogram(query_descriptors,humanTemplates[init_id].humanDescriptors,clustersOrg);
#else
        matching_percentage = surfDesc_Matching1(query_descriptors,query_keypoints,humanTemplates[init_id].humanDescriptors,tempK);
#endif

#if(!HISTOGRAM)
        if(matching_percentage < TREE_CRITERIA_THRESHOLD)
#else
        if(matchVal*100 > HIST_MATCH_THRESH)
#endif
        {
            if(nodeArray[init_id]->left != NULL)
            {
                init_id = nodeArray[init_id]->left->data;
            }
            else
            {
                // attach is on the left
                nodeArray[init_id]->left = nodeArray[nodeNo];
                nodeArray[nodeNo]->parent = nodeArray[init_id];
                nodeArray[nodeNo]->left = NULL;
                nodeArray[nodeNo]->right = NULL;

                break;
            }
        }
        else
        {
            if(nodeArray[init_id]->right != NULL)
            {
                init_id = nodeArray[init_id]->right->data;
            }
            else
            {
                nodeArray[init_id]->right = nodeArray[nodeNo];
                nodeArray[nodeNo]->parent = nodeArray[init_id];
                nodeArray[nodeNo]->left = NULL;
                nodeArray[nodeNo]->right = NULL;
                break ;
            }
        }

    }
    return ;
}

#endif // TRACKER_H
