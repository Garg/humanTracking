/*! @file */
#ifndef TRACKFUNCTIONS_H
#define TRACKFUNCTIONS_H

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include <opencv2/ml/ml.hpp>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include<sys/time.h>
#include<opencv/cv.h>
#include<opencv/highgui.h>
#include<dai/bp.h>
#include<dai/factorgraph.h>

/*! If using ROS, enable it 1. This is only being used for generating time duration for the algorithm. */
#define ROS     0

#if(ROS)
#include<ros/time.h>
#endif

/*! 1 for enabling only torso-leg tracking instead of whole human.*/
#define GMM_NO_HEAD         0

/*! If window is scaled this much, then select template. */
#define CHANGE_IN_SCALING 10

/*! Area thresholds for individual connected components in any body part.*/
#define HEAD_AREA 500
#define TORSO_AREA 500
#define LEG_AREA 100

/*! Search window to be scaled with this factor for further matching. */
#define EXTRA_SCALING_FACTOR 1.2

/*! Scaling allowed between the template and query.*/
#define SCALE_ALLOWED       20

/*! 1 for viewing images that are temporarily made and displayed for debugging. */
#define RESULTS_IMAGES      0

/*! 1 for verbose logs for debugging. */
#define VERBOSE             0

/*! 1 for enabling GMM based segmentation method instead of K-means cluster based.*/
#define GMM                 1

/*! 1 to stop all displays. */
#define NO_IMGPROC          0

/*! 1 to use histogram matching instead of SURF matching in tree.*/
#define HISTOGRAM           1

/*! Historam matching threshold for searching and attaching node in tree.*/
#define HIST_MATCH_THRESH   7

/*!  Minimum matches in SURF to consider the matching.*/
#define MIN_MATCH_COUNT     5

/*! Select video or image sequence for input.*/
#define VIDEO               1

/*! 1 for Search with tree otherwise 0 for lasttemplate.*/
#define TREE                1

/*! Threshold for deciding position of a new node in tree.*/
#define TREE_CRITERIA_THRESHOLD 10

/*! SURF matching threshold for fetching a node template in tree.*/
#define MATCHING_THRESHOLD 10

/*! Number of clusters in K-Means for image segmentation.*/
#define NO_OF_COLOR_CLUSTERS 9

/*! RANSAC threshold for homography, range from 1 to 10. */
#define RANSAC_THREHOLD         1

using namespace cv;
using namespace std;
using namespace dai;

void mouseCB_selectParts( int event, int x, int y, int flags, void* param );
void mouseCB_polypoints( int event, int x, int y, int flags, void* param );
void my_mouse_callback( int event, int x, int y, int flags, void* param );

void segmentBodyParts(EM em1, EM em2, EM em3, EM em4, Mat samples, Mat& labels, int no_head);
void searchBin ( Mat& desc2, Mat& clusters, Mat& labels2, int& NOC, vector<int> bodyLabels);

void shiftPoints( vector<Point2f>& points, CvRect roi);
void shiftPoints_G2L( vector<Point2f>& points, CvRect roi);
void shiftKeyPoints( vector<KeyPoint>& points, CvRect roi); // shift keypoints ROI frame to global frame
void appendMatrixHorz(Mat mat1, Mat mat2, Mat& matOut);
void boundaryCheckRectOuter(Rect& box);
void checkBoundaryRect(Rect &boxRect);
void checkBoundaryRect(Rect &boxRect, Size size);
void extraScaleRectangle(Rect &scaledRect,float scaleFactor);

void crossCheckMatching( Ptr<DescriptorMatcher>& descriptorMatcher,
                         const Mat& descriptors1, const Mat& descriptors2,
                         vector<DMatch>& filteredMatches12, int knn );
double surfDesc_Matching2(Mat img1,Mat &temp1Desc, vector<KeyPoint> &temp1Keypoint,Mat img2, Mat& temp2Desc,
                          vector<KeyPoint>& temp2Keypoint, vector<Point2f> &prevMatchPoints, vector<Point2f> &currentMatchPoints,
                          Mat &currentMatchDesc, Rect Box, Point2f &centerSURF, int &numMatch);
int getMatcherFilterType( const string& str);
void scalebox(Rect & box, Rect boxSrc, vector<Point2f>& p12, vector<Point2f>& p22, double & scale, double AR, int sr,
              Point2f &centerSurf, Rect boxOrg);
void weightScalingAspect ( vector<Point2f>& matchPoints1, vector<Point2f>& matchPoints2, double *overallScale,int sr);
double surfDesc_Matching1(Mat &temp1Desc, vector<KeyPoint> temp1Keypoint, Mat& temp2Desc,vector<KeyPoint>& temp2Keypoint);


void eye(Mat& P, int k);
void multiply(Mat& P, double k);

void getFactors(ofstream& factorFile, Mat_<int> adjMat, vector<vector<Point2f> > candidates,
                vector<Mat_<float> > &edgePotentials);
void findMatch_MRF_MAP(Mat_<int> adjMat, vector< vector<Point2f> > candidates, vector<Point2f>& outPoints,
                       vector<int>& partsFound, vector<int> &matchIndex);

void trainGMM(EM& emModel, Mat sampleInput, int clusNum);

double matchHistogram(Mat desc1, Mat desc2, Mat clusterCenters );

#endif // TRACKFUNCTIONS_H
