/*! \file
    @mainpage A novel SURF-based algorithm for tracking human from mobile robot platform.
  * @section Introduction
  * Detecting and tracking a human from a mobile
    robot platform has several applications in service robotics where
    a robot is expected to assist humans. In this paper, we propose
    a novel interest point-based algorithm that can track a human
    reliably under several challenging situations like variation in
    illumination, pose change, scaling, camera motion and occlusion.
    The limitations of point-based methods are overcome using colour
    information and imposing a structure on the colour blobs. When-
    ever sufficient number of SURF matching points are not available
    for a given frame, the presence of human is detected using Markov
    random field based graph matching algorithm. Imposition of
    structure on coloured blobs helps in eliminating background
    objects having similar colour distribution. The stability-versus-
    plasticity dilemma inherent in a tracking over long run is resolved
    by selecting new templates on-line and maintaining a fixed-size
    tree of templates which is updated with new information. The
    performance of the algorithm is demonstrated through simulation
    on standard datasets and the computation time is found to be
    comparable with existing SURF-based tracking methods.

    @section algo_flowchart Algorithm
    \image latex flowchart.eps "Flowchat"
    \image html flowchart.jpg "Flowchart"

    @section code_howTo Using the code

    @subsection requirements Requirements
    @subsubsection os OS
    Ubuntu 14.04 with Robot Operating System (ROS fuerte) environment settings.
    @subsubsection ide IDE
    Qt4
    @subsubsection depends Dependency
    -# OpenCv - 2.4.9 - This is used through ROS environment and the dependency has been mentioned in manifest.xml. User can
    use opencv as an external library also if ROS environment is not being used.
    -# LibDAI - 0.3.2 - This is used as an external library. The path for the same needs to be set in CMakeLists.txt. This is required for
    graph matching using MRF-MAP based solution.
    @subsection Compile
    $ cd build
    $ cmake ..
    $ make
    @subsection Run
    $ cd bin
    $ ./humanTracking
    <Select the boundary of human to be tracked by left mouse clicking to form a polygon around human>
    <Right click when selection completes>
    @subsection Input
    Input video /bin/d.avi. The initial selection is already set in the code, which can be replaced by mouse based
    selection of the human body. After that, the segmented body image needs three left clicks to specify that we are using
    the code in the full human mode. If only torso and legs are tracked, then two left clicks and one right click is
    required along with the corresponding macro set to 1.
    @subsection Output
    The output tracked video is stored in /bin/trackVideo.avi. Some data files are generated as well.

  */

#include<sc_tracking2/tracker.h>

using namespace std;


int main(int argc,char* argv[])
{
#if(ROS)
    ros::Time::init();
    ros::Duration sumTime;
#endif
    ofstream timerFile("timer.txt");
    float avgTime = 0.0;

    startFlag = true;

    Tracker *tracker1 =  new Tracker();

    startFlag = false;

    for(int s=0;s<1000;s++)
    {
#if(ROS)
        ros::Time t1 = ros::Time::now();
#endif
        char imageName[200];

        if(VIDEO==2)
            sprintf(imageName,"./dataset/seq03-img-right/image_00000%d_1.png",s+101);
        if(VIDEO==3)
            sprintf(imageName,"./dataset/seq00-img-left/image_00000%d_0.png",s+181);
        if(VIDEO==4)
            sprintf(imageName,"./dataset/seq05-img-left/image_00000%d_0.png",s+101);


        Mat frame;

        frame = imread(imageName);

        if(VIDEO==1)
            tracker1->cap >> frame;

        if(frame.empty())
        {
            cout << "empty frame read " << endl;
            tracker1->writer.release();
            exit(-1);
        }


        tracker1->currentImg = frame.clone();

        tracker1->trackHuman();

        tracker1->prevImg = tracker1->currentImg.clone();
#if(ROS)
        ros::Duration dur1 = ros::Time::now() - t1;

        sumTime += dur1;
        avgTime = ((avgTime * s) + dur1.toSec())/(s+1);

        timerFile << s+1 << "\t" << dur1.toSec() << "\t" << avgTime << endl;
#endif
    }

    //Section for writing a code for treeFile

    ofstream treeFile;
    treeFile.open("tree.dot");

    if(!treeFile.is_open())
    {
        cerr << "error opening treedot file" << endl;
        exit(0);
    }


    treeFile << "graph graphname {" << endl;

    for(int i=0;i<tracker1->totalTemplates;i++)
    {
        if(tracker1->nodeArray[i]->left != NULL)
        {
            treeFile << tracker1->nodeArray[i]->data << " -- " << tracker1->nodeArray[i]->left->data << "[color=blue];" << endl;
        }
        if(tracker1->nodeArray[i]->right != NULL)
        {
            treeFile << tracker1->nodeArray[i]->data << " -- " << tracker1->nodeArray[i]->right->data << "[style=dotted];" << endl;
        }
    }

    treeFile << "}" << endl;
    treeFile.close();

    tracker1->~Tracker();
    return 0;
}
