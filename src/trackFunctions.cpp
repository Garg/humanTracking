/*! @file */
#include <sc_tracking2/trackFunctions.h>

extern Rect box_selection;
extern bool drawing_box ;
extern bool rect_drawn ;
extern bool drawing_poly ;
extern bool poly_drawn ;

extern int partSelected;
extern vector<Point2f> partPoints;
extern vector<Point2f> bodyPointsSelected;
extern bool pointSelectionDone;

/*!
  @brief Mouse Callback function for selecting color clusters corresponding to the different body
  parts on which graph should be constructed.
  */
void mouseCB_selectParts( int event, int x, int y, int flags, void* param )
{
    switch( event ){

    case CV_EVENT_LBUTTONDOWN:
        partSelected++;
        partPoints.push_back(Point2f(x,y));
        break;

    case CV_EVENT_RBUTTONDOWN:
        partSelected++;
        partPoints.push_back(Point2f(0,0));
        break;
    }
}

/*!
  @brief Mouse callback function for selecting input points on the body in the first frame
  */
void mouseCB_polypoints( int event, int x, int y, int flags, void* param ){


    switch( event ){

    case CV_EVENT_LBUTTONDOWN:
        bodyPointsSelected.push_back(Point2f(x,y));
//        cout << "bodyPointsSelected.push_back(Point2f("<<x<<","<<y<<"));"<<endl;
        break;

    case CV_EVENT_RBUTTONDOWN:
        pointSelectionDone = true;
        break;
    }
}

/*!
  @brief Mouse Callback function for selecting a rectangular ROI on the body in the first frame.
  */

void my_mouse_callback( int event, int x, int y, int flags, void* param ){


    switch( event ){
    case CV_EVENT_MOUSEMOVE:
        if( drawing_box ){
            box_selection.width = x-box_selection.x;
            box_selection.height = y-box_selection.y;
        }
        break;

    case CV_EVENT_LBUTTONDOWN:
        drawing_box = true;
        box_selection = cvRect( x, y, 0, 0 );
        break;

    case CV_EVENT_LBUTTONUP:
        drawing_box = false;
        rect_drawn=true;
        if( box_selection.width < 0 ){
            box_selection.x += box_selection.width;
            box_selection.width *= -1;
        }
        if( box_selection.height < 0 ){
            box_selection.y += box_selection.height;
            box_selection.height *= -1;
        }
        break;
    }
}

void boundaryCheckRectOuter(Rect& box)
{
    if (box.x < 0)
        box.x = 0;

    if ((box.x + box.width) > 639 )
        box.width = 639-box.x;

    if (box.y < 0)
        box.y = 0;

    if ((box.y + box.height) > 479 )
        box.height = 479-box.y;
}

/*!
  @brief Shifts points from local to the global domain.
  */

void shiftPoints( vector<Point2f>& points, CvRect roi)
{
    Point2f shift(roi.x, roi.y);
#if DEBUG
    cout << "Shifting Points from ROI to whole Image..." << endl;
#endif
    for ( unsigned int i=0; i < points.size(); i++)
        points[i] += shift;
}

/*!
  @brief Shifts points from global to the domain specified by the rectangle roi.
  */

void shiftPoints_G2L( vector<Point2f>& points, CvRect roi)
{
    Point2f shift(roi.x, roi.y);
    for ( unsigned int i=0; i < points.size(); i++)
        points[i] -= shift;
}

/*!
  @brief Shifts keypoints from local to global frame
  */
void shiftKeyPoints( vector<KeyPoint>& points, CvRect roi)
{
    Point2f shift(roi.x, roi.y);
#if DEBUG
    cout << "Shifting Points from ROI to whole Image..." << endl;
#endif
    for ( unsigned int i=0; i < points.size(); i++)
    {
        points[i].pt.x += shift.x;
        points[i].pt.y += shift.y;
    }
}

/*!
  @brief Finds cross check matching between the descriptors
  */
void crossCheckMatching( Ptr<DescriptorMatcher>& descriptorMatcher,
                         const Mat& descriptors1, const Mat& descriptors2,
                         vector<DMatch>& filteredMatches12, int knn=1 )
{
    filteredMatches12.clear();
    assert(!descriptors1.empty());
    vector<vector<DMatch> > matches12, matches21;
    descriptorMatcher->knnMatch( descriptors1, descriptors2, matches12, knn );
    descriptorMatcher->knnMatch( descriptors2, descriptors1, matches21, knn );
    for( size_t m = 0; m < matches12.size(); m++ )
    {
        bool findCrossCheck = false;
        for( size_t fk = 0; fk < matches12[m].size(); fk++ )
        {
            DMatch forward = matches12[m][fk];

            for( size_t bk = 0; bk < matches21[forward.trainIdx].size(); bk++ )
            {
                DMatch backward = matches21[forward.trainIdx][bk];
                if( backward.trainIdx == forward.queryIdx )
                {
                    filteredMatches12.push_back(forward);
                    findCrossCheck = true;
                    break;
                }
            }
            if( findCrossCheck ) break;
        }
    }
}

/*!
  @brief Finds SURF matching between the two images.
  */
double surfDesc_Matching2(Mat img1,Mat &temp1Desc, vector<KeyPoint>& temp1Keypoint,Mat img2, Mat& temp2Desc,vector<KeyPoint>& temp2Keypoint,
                          vector<Point2f> &prevMatchPoints, vector<Point2f> &currentMatchPoints,Mat &currentMatchDesc, Rect Box,
                          Point2f& centerSURF, int& numMatch)
{
    vector<int> queryIdxs,  trainIdxs;
    vector<DMatch> filteredMatches;


    Ptr<DescriptorMatcher> descriptorMatcher;
    descriptorMatcher = DescriptorMatcher::create( "FlannBased" );

    Mat temp22Desc;

    vector<KeyPoint> temp22Keypoint;
    vector<Point2f> ptemp;
    KeyPoint::convert(temp2Keypoint,ptemp);
    for(int i=0;i<ptemp.size();i++)
    {
        if(Box.contains(ptemp[i]))
        {
            temp22Desc.push_back(temp2Desc.row(i));
            temp22Keypoint.push_back(temp2Keypoint[i]);
        }
    }


    crossCheckMatching( descriptorMatcher, temp1Desc, temp22Desc, filteredMatches, 1 );

    for( size_t i = 0; i < filteredMatches.size(); i++ )
    {
        queryIdxs.push_back(filteredMatches[i].queryIdx);
        trainIdxs.push_back(filteredMatches[i].trainIdx);
    }

    Mat pointsTransed; // Points for inverse homography
    Mat H12;

    if( RANSAC_THREHOLD >= 0 )
    {
        vector<Point2f> points1; KeyPoint::convert(temp1Keypoint, points1, queryIdxs);
        vector<Point2f> points2; KeyPoint::convert(temp22Keypoint, points2, trainIdxs);
        if (points2.size() < 4 )
            return 0;
        H12 = findHomography( Mat(points1), Mat(points2), CV_RANSAC, RANSAC_THREHOLD );
    }

    Mat drawImg;

    double percentage_match = 0.0;

    if( !H12.empty() )
    {
        vector<char> matchesMask( filteredMatches.size(), 0 );
        vector<Point2f> points1; KeyPoint::convert(temp1Keypoint, points1, queryIdxs);
        vector<Point2f> points2; KeyPoint::convert(temp22Keypoint, points2, trainIdxs);

        Mat points1t; perspectiveTransform(Mat(points1), points1t, H12);


        int count = 0;
        for( size_t i1 = 0; i1 < points1.size(); i1++ )
        {
            if( ( norm(points2[i1] - points1t.at<Point2f>((int)i1,0)) <= 10 ))
            {
                count++;
                matchesMask[i1] = 1;
            }
        }

        for( size_t i1 = 0; i1 < points1.size(); i1++ )
        {
            if(matchesMask[i1]==1)
            {
                prevMatchPoints.push_back(points1[i1]);
                currentMatchPoints.push_back(points2[i1]);
                currentMatchDesc.push_back(temp22Desc.row(i1));
            }
        }

        // Find the new center using perspective transform-----------
        vector<Point2f> centerVec;
        Mat centerVecOut;
        centerVec.push_back(centerSURF);
        perspectiveTransform((Mat)centerVec, centerVecOut, H12);
        centerSURF = centerVecOut.at<Point2f>(0);
        //---------------

        drawMatches( img1, temp1Keypoint, img2, temp22Keypoint, filteredMatches, drawImg, CV_RGB(0, 255, 0), CV_RGB(0, 0, 255), matchesMask);

        percentage_match = (double)(count*100)/temp1Desc.rows;
        numMatch = count;
        char textStr[100];
        sprintf(textStr," %d ", (int)percentage_match);
        putText(drawImg,textStr,Point2f(drawImg.cols-100,20),1,2,Scalar(0,0,255),2,CV_AA);

        points1.clear();points2.clear();


    }

    if(!NO_IMGPROC)
        imshow("drawImg",drawImg);

    queryIdxs.clear(); trainIdxs.clear(); filteredMatches.clear();
    temp22Keypoint.clear();temp22Desc.release();
    pointsTransed.release();    drawImg.release();H12.release();

    return percentage_match;

}

/*!
  @brief Determines the type of filter
  */
int getMatcherFilterType( const string& str )
{
    if( str == "NoneFilter" )
        return 0;
    if( str == "CrossCheckFilter" )
        return 1;
    CV_Error(CV_StsBadArg, "Invalid filter name");
    return -1;
}

/*!
  @brief Finds labels for the samples, given the Gaussian models of the body parts
  @param[in] em1    EmModel for head if no_head is zero and not used if no_head is 1.
  @param[in] em2    EmModel for torso.
  @param[in] em3    EmModel for legs.
  @param[in] em4    EmModel for background.
  @param[in] samples    Input samples for labelling.
  @param[out] labels    Output labels for the samples.
  @param[in] no_head    Flag for determining with/withoud head segmentation of body.
  */
void segmentBodyParts(EM em1, EM em2, EM em3, EM em4, Mat samples, Mat& labels, int no_head)
{
    samples.convertTo(samples,CV_64FC1);

    RNG randIndex;
    Mat samples2;
    Mat_<int> flag(samples.rows,1,-1);
    for(int k=0; k<samples.rows/4; k++)
    {
        int index = randIndex.uniform(0,samples.rows);
        samples2.push_back(samples.row(index));
        flag(index) = 1;
    }

    for(int k=0; k<samples.rows; k++)
    {
        if(flag(k)==-1)
        {
            labels.push_back(-1);
            continue;
        }

        Mat probs;

        Vec2d out1 = em1.predict(samples.row(k), probs);
        Vec2d out2 = em2.predict(samples.row(k),probs);
        Vec2d out3 = em3.predict(samples.row(k), probs);
        Vec2d outBG = em4.predict(samples.row(k), probs);

        double likelyhd1 = std::exp(out1[0]);
        double likelyhd2 = std::exp(out2[0]);
        double likelyhd3 = std::exp(out3[0]);
        double likelyhdBG = std::exp(outBG[0]);

        double prob1_ = likelyhd1/(likelyhd1 + likelyhd2 + likelyhd3 + likelyhdBG);
        double prob2_ = likelyhd2/(likelyhd1 + likelyhd2 + likelyhd3 + likelyhdBG);
        double prob3_ = likelyhd3/(likelyhd1 + likelyhd2 + likelyhd3 + likelyhdBG);

        if(no_head)
        {
            float thresh = 0.5;
            if( prob2_ > thresh)
            {
                labels.push_back(0);
            }
            else if( prob3_ > thresh)
            {
                labels.push_back(1);
            }
            else
                labels.push_back(-1);
        }
        else
        {
            float thresh = 0.5;
            if( prob1_ > thresh)
            {
                labels.push_back(0);
            }
            else if( prob2_ > thresh)

            {
                labels.push_back(1);
            }
            else if( prob3_ > thresh)

            {
                labels.push_back(2);
            }
            else
                labels.push_back(-1);
        }
    }

}

/*!
  @brief Finds labels for the input samples, given the cluster centers
  */
// searchBin ( Input matrix of descriptors, Input clusters centers to be matched to, Output Matrix of labels for descriptors)
void searchBin ( Mat& desc2, Mat& clusters, Mat& labels2, int& NOC, vector<int> bodyLabels)
{
    float distFromCluster, minDis=999, clusterThresh ;
    // eachIter << "searchBin- MindistFromCluster for " << desc2.rows << " points" << endl;
    if (desc2.rows <= NOC)
        ;
    // cout << "Warning: Points less than noc" << endl;
    for (int i = 0; i < desc2.rows; i++)
    {
        int index = -1;
        minDis = 999;
        for (int j = 0; j < clusters.rows; j++)
        {
            clusterThresh = 50;

            distFromCluster = norm(desc2.row(i) - clusters.row(j));

            if (distFromCluster < minDis && distFromCluster < clusterThresh)
            {
                minDis = distFromCluster;
                index = j;
            }
        }
        //        labels2.push_back(bodyLabels[index]);
        labels2.push_back(index);
        // eachIter << minDis << endl;
    }
}

void eye(Mat& P, int k)
{
    for(int i=0;i<P.rows;i++)
        for(int j=0;j<P.cols;j++)
        {
            if(i==j)
                P.at<float>(i,j) = k*1.0;
            else
                P.at<float>(i,j) = 0.0;
        }

}
void multiply(Mat& P, double k)
{
    for(int i=0;i<P.rows;i++)
        for(int j=0;j<P.cols;j++)
            P.at<float>(i,j) = k* P.at<float>(i,j);
}

void scalebox(Rect & box, Rect boxSrc, vector<Point2f>& p12, vector<Point2f>& p22, double & scale, double AR, int sr,
              Point2f& centerSurf, Rect boxOrg)
{
    //    if(p22.size() > 2)
    {
        //        Rect R1;

        //        R1 = boundingRect(p22);
        shiftPoints_G2L(p12,boxSrc);
        shiftPoints_G2L(p22,box);

        double scaleTemp = scale;
        weightScalingAspect(p12,p22,&scale,sr);
        //        scale *= scaleTemp;
        shiftPoints(p12,boxSrc);
        shiftPoints(p22,box);
        //        cout << " scale factor \t" << scale << endl;
        //        float ARorg = (float)boxOrg.width/(float)boxOrg.height;

        box.height = int(boxSrc.height*scale);
        box.width = AR*box.height;

        box.y = int(centerSurf.y -box.height/2);
        box.x = int(centerSurf.x -box.width/2);

        //        box.y = int(R1.y + R1.height/2 -0.35*box.height );
        //        box.x = int(R1.x + R1.width/2 -box.width/2);

        boundaryCheckRectOuter(box);
    }
    //    else
    //        return;
}


void weightScalingAspect ( vector<Point2f>& matchPoints1, vector<Point2f>& matchPoints2, double *overallScale,int sr)
{
    int SCALE_RANGE = sr;
    int count=0, ind=0, num = matchPoints1.size();
    vector<double> scale;

    if (matchPoints1.size() > 1 )
    {
        for (int i = 0; i < num; i++)
            for (int j=i+1; j< num; j++)
            {

                if ((matchPoints1[i].x - matchPoints1[j].x) != 0 && (matchPoints1[i].y - matchPoints1[j].y) != 0)
                {
                    scale.push_back(norm(matchPoints2[i] - matchPoints2[j])/norm(matchPoints1[i] - matchPoints1[j]));

                    if ( scale[count]< (1+float(SCALE_RANGE)/100) && scale[count] > (1-float(SCALE_RANGE)/100) )
                    {
                        // scaleData << count<<"\t"<<scale[count]<<"\t"<<weight[count]<<"\t"<<matchPoints1[i]<<"\t"<<matchPoints1[j]<<"\t"<<matchPoints2[i]<<"\t"<<matchPoints2[j]<<endl;
                        count++;
                    }
                    else
                        scale.pop_back();
                }
            }


        *overallScale=0;

        // Computing overall scaling for the new ROI
        for (int i=0; i<count; i++ )
            *overallScale += scale[i];

        if(count > 1)
            *overallScale /= count;
        else
            *overallScale =1;

    }
    else
    {
        cout << "MatchPoints less than 3 while calculating Scaling" << endl;
        *overallScale=1;
    }
}

double surfDesc_Matching1(Mat &temp1Desc, vector<KeyPoint> temp1Keypoint, Mat& temp2Desc,vector<KeyPoint>& temp2Keypoint)
{
    vector<int> queryIdxs,  trainIdxs;
    vector<DMatch> filteredMatches;


    Ptr<DescriptorMatcher> descriptorMatcher;
    descriptorMatcher = DescriptorMatcher::create( "FlannBased" );

    cout << temp1Desc.rows << "\t" << temp2Desc.rows << endl;
    crossCheckMatching( descriptorMatcher, temp1Desc, temp2Desc, filteredMatches, 1 );

    for( size_t i = 0; i < filteredMatches.size(); i++ )
    {
        queryIdxs.push_back(filteredMatches[i].queryIdx);
        trainIdxs.push_back(filteredMatches[i].trainIdx);
    }

    Mat pointsTransed; // Points for inverse homography
    Mat H12;

    if( RANSAC_THREHOLD >= 0 )
    {
        vector<Point2f> points1; KeyPoint::convert(temp1Keypoint, points1, queryIdxs);
        vector<Point2f> points2; KeyPoint::convert(temp2Keypoint, points2, trainIdxs);
        if (points2.size() < 4 )
            return 0;
        H12 = findHomography( Mat(points1), Mat(points2), CV_RANSAC, RANSAC_THREHOLD );
    }

    Mat drawImg;

    double percentage_match = 0.0;

    if( !H12.empty() )
    {
        vector<char> matchesMask( filteredMatches.size(), 0 );
        vector<Point2f> points1; KeyPoint::convert(temp1Keypoint, points1, queryIdxs);
        vector<Point2f> points2; KeyPoint::convert(temp2Keypoint, points2, trainIdxs);

        Mat points1t; perspectiveTransform(Mat(points1), points1t, H12);


        int count = 0;
        for( size_t i1 = 0; i1 < points1.size(); i1++ )
        {
            if( ( norm(points2[i1] - points1t.at<Point2f>((int)i1,0)) <= 20 ))
            {
                count++;
                matchesMask[i1] = 1;
            }
        }

        percentage_match = (double)(count*100)/temp1Desc.rows;
        points1.clear();points2.clear();
    }


    queryIdxs.clear(); trainIdxs.clear(); filteredMatches.clear();
    pointsTransed.release();    drawImg.release();H12.release();

    return percentage_match;

}

/*!
    @brief Append matrix horizontally with another matrix
    @param mat1 - Left matrix
    @param mat2 - right matrix
    @param matOut - Appended output matrix
*/
void appendMatrixHorz(Mat mat1, Mat mat2, Mat& matOut)
{
    int numRows = mat1.rows > mat2.rows ? mat1.rows : mat2.rows;
    int numCols = mat1.cols + mat2.cols;

    matOut = Mat::zeros(numRows,numCols,CV_8UC3);

    mat1.copyTo(matOut(Rect(0,0,mat1.cols,mat1.rows)));
    mat2.copyTo(matOut(Rect(mat1.cols,0,mat2.cols,mat2.rows)));
}

/*!
    @brief Checks boundary conditions on the rectangle against the image size and snips it at the boundary
    (AR not maintained)
  */
void checkBoundaryRect(Rect &boxRect, Size size)
{
    if(boxRect.x < 0)
        boxRect.x = 0;
    if(boxRect.y < 0)
        boxRect.y = 0;

    if(boxRect.y + boxRect.height > size.height)
    {
        if(boxRect.height > size.height)
        {
            boxRect.y = 0;
            boxRect.height = size.height;
        }
        else if(boxRect.height + boxRect.y > size.height)
        {
            //            boxRect.y = size.height - boxRect.height;
            boxRect.height -= (boxRect.height + boxRect.y - size.height);
        }
    }

    if(boxRect.x + boxRect.width > size.width)
    {
        if(boxRect.width > size.width)
        {
            boxRect.x = 0;
            boxRect.width = size.width;
        }
        else if(boxRect.width + boxRect.x > size.width)
        {
            //            boxRect.x = size.width - boxRect.width;
            boxRect.width -= (boxRect.width + boxRect.x - size.width);
        }
    }

    return ;

}

void checkBoundaryRect(Rect &boxRect)
{
    if(boxRect.x < 0)
        boxRect.x = 0;
    if(boxRect.y < 0)
        boxRect.y = 0;

    if(boxRect.y + boxRect.height > 480)
    {
        if(boxRect.height > 480)
        {
            boxRect.y = 0;
            boxRect.height = 480;
        }
        else if(boxRect.height + boxRect.y > 480)
        {
            boxRect.y = 480 - boxRect.height;
            boxRect.height = boxRect.height;
        }
    }

    if(boxRect.x + boxRect.width > 640)
    {
        if(boxRect.width > 640)
        {
            boxRect.x = 0;
            boxRect.width = 640;
        }
        else if(boxRect.width + boxRect.x > 640)
        {
            boxRect.x = 640 - boxRect.width;
            boxRect.width = boxRect.width;
        }
    }

    return ;

}
// Scaling should be done such the center of the new rectangle remains at the same point;
void extraScaleRectangle(Rect &scaledRect,float scaleFactor)
{

    int width = scaledRect.width;
    int height = scaledRect.height;

    //    cout << "SCALED \t" << scaleFactor << endl;
    //    cout << "before \t " << scaledRect.x << "\t" << scaledRect.y << endl;
    //    cout << scaledRect.width << "\t" << scaledRect.height << endl;

    scaledRect.width = int(scaleFactor * width);
    scaledRect.height = int(scaleFactor * height);

    int widthDiff = scaledRect.width - width;
    int heightDiff = scaledRect.height - height;

    scaledRect.x = int(scaledRect.x - (float)widthDiff/2);
    scaledRect.y = int(scaledRect.y - (float)heightDiff/2);

    checkBoundaryRect(scaledRect);

    //    cout << "after\t " << scaledRect.x << "\t" << scaledRect.y << endl;
    //    cout << scaledRect.width << "\t" << scaledRect.height << endl;


    return;


}

/*!
    @brief Function for finding the factors for a given adjacenecy matrix of a graph
    @param[in,out] factorFile   stores the factors in particular format in this file
    @param[in] adjMat           adjacency matrix of the graph
    @param[in] candidates       candidates for the nodes of graph
    @param[out] edgePotentials  stores edge potentials for each possible edge, in order to calculate the error of the selected edge
*/
void getFactors(ofstream& factorFile, Mat_<int> adjMat, vector< vector<Point2f> > candidates,
                vector< Mat_<float> >& edgePotentials)
{
    // Iterate over the adjacency matrix (upper triangualar part only)
    for(uint i=0; i<adjMat.rows; i++)
        for(uint j=i+1; j<adjMat.cols; j++)
        {
            // only if there is a connection between two nodes
            if(adjMat[i][j]!=-1)
            {
                Mat_<float> truthTable(candidates[i].size(),candidates[j].size(),0.0);
                // for this particular case, the required angle between nodes is -90
                float theta1 = -90;

                factorFile << 2 << endl; // Number of variables in the factor
                factorFile << i << " " << j << endl; // Indicies of those variables
                factorFile << candidates[i].size() << " " << candidates[j].size() << endl; // Cardinality of each variable
                factorFile <<  candidates[i].size() * candidates[j].size() << endl; // Non Zero states

                int factorCounter=0;

                // Iterate over all the possible candidates for given pair of nodes
                for(int m=0; m<candidates[j].size(); m++)
                {
                    //                    candidateError[j][m] = 0;
                    for(int l=0; l<candidates[i].size(); l++)
                    {
                        //                        candidateError[i][l] = 0;
                        {
                            // Find the angle between candidate nodes
                            float theta2 = float(180 * 7.0/22.0) *
                                    ( atan2( (candidates[i][l].y - candidates[j][m].y), (candidates[i][l].x - candidates[j][m].x) ) );

                            if(isnan(theta2))
                                theta2 = -90;

                            // Calcualte edge potential
                            float psi = exp(- 5*abs(theta1-theta2)/100 );

                            // if it is too less, punish further, possibly an outlier
                            if(psi < 0.6)
                            {
                                psi = 0.0001;
                            }

                            factorFile << factorCounter << " " << psi << endl; // factor state index and value

                            truthTable(l,m) = psi;

                            //                            cout << i << "\t" << j << "\t" << l << "\t" << m << "\t" <<
                            //                                    theta2 << "\t" << psi << "\t" << abs(theta1-theta2) << endl;

                            factorCounter++;
                        }
                    }
                }
                edgePotentials.push_back(truthTable);

                factorFile << "\n" << endl;
            }
        }
}

/*!
    @brief Finds graph matching based on MAP solution using BP for a given MRF model of an undirected graph
    @param[in] adjMat   adjacency matrix of the graph (n x n, n is number of nodes of graph)
    @param[in] candidates   observed candidates for each node of graph
    @param[out] outpoints   selected candidates
    @param[out] partsFound  flags vector for nodes found or not found
    @param[out] matchIndex  indicies of selected candidates
*/
void findMatch_MRF_MAP(Mat_<int> adjMat, vector< vector<Point2f> > candidates, vector<Point2f>& outPoints, vector<int>& partsFound,
                       vector<int>& matchIndex)
{
    // File to store the factors in a particular format
    ofstream factorFile;
    factorFile.open("factor.fg");

    factorFile << adjMat.rows-1 << "\n" << endl;

    vector< Mat_<float> > edgePotentials;

    // Function for calculating factors
    getFactors(factorFile,adjMat,candidates,edgePotentials);

    // Reading factors from the file (written above)
    FactorGraph fg;
    fg.ReadFromFile("factor.fg");

    // Set some constants
    size_t maxiter = 10000;
    Real   tol = 1e-9;
    size_t verb = 1;

    // Store the constants in a PropertySet object
    PropertySet opts;
    opts.set("maxiter",maxiter);  // Maximum number of iterations
    opts.set("tol",tol);          // Tolerance for convergence
    opts.set("verbose",verb);     // Verbosity (amount of output generated)

    // Construct a BP (belief propagation) object from the FactorGraph fg
    // using the parameters specified by opts and two additional properties,
    // specifying the type of updates the BP algorithm should perform and
    // whether they should be done in the real or in the logdomain
    //
    // Note that inference is set to MAXPROD, which means that the object
    // will perform the max-product algorithm instead of the sum-product algorithm
    BP mp(fg, opts("updates",string("SEQMAX"))("logdomain",false)("inference",string("MAXPROD"))("damping",string("0.1")));
    // Initialize max-product algorithm
    mp.init();
    // Run max-product algorithm
    mp.run();
    // Calculate joint state of all variables that has maximum probability
    // based on the max-product result
    vector<size_t> mpstate = mp.findMaximum();

    for( size_t i = 0; i < mpstate.size(); i++ )
    {
        // Store the output points selected by MAP solution (mpstate stores the selected candidate index)
        outPoints[i] = candidates[i][mpstate[i]];
        matchIndex[i] = mpstate[i];
    }

    // Check if selected edgePotential is in range
    for(int k=0; k<adjMat.rows-1; k++)
    {
        if(edgePotentials[k](mpstate[k],mpstate[k+1]) < 0.1)
        {
            partsFound[k] = -1;
        }
    }
}

/*!
  @brief Function for training GMM model based on EM.
  @param[in,out] EmModel    Model for training.
  @param[in] sampleInput    Input samples for training.
  @param[in] clusNum        Number of clusters/components in the model.
  */
void trainGMM(EM& emModel, Mat sampleInput, int clusNum)
{
    vector<Mat> covsInit;//(clusNum, Mat::eye(dims, dims, CV_64FC1)/10);
    Mat meansInput = Mat::zeros(clusNum, sampleInput.cols, CV_64FC1);
    Mat weightsInput = Mat::ones(1, clusNum, CV_64FC1)/clusNum;
    Mat out1, out2, out3;

    cout << "Training GMM model" << endl;
    emModel.trainE(sampleInput, meansInput, covsInit, weightsInput, out1, out2, out3);
    FileStorage fs;
    fs.open("emModel1.txt",FileStorage::WRITE);
    emModel.write(fs);
    cout << "Training Complete" << endl;

}

/*!
    @\brief Finds histogram matching between two sets of descriptors w.r.t. fixed cluster centers
    @param[in] desc1    Input descriptors 1.
    @param[in] desc2    Input descriptors 2.
    @param[in] clusterCenters Input cluster centers (bins for histogram) w.r.t. which histograms are to be built.
    @return Hellinger distance between the histograms of descriptors.
  */
double matchHistogram(Mat desc1, Mat desc2, Mat clusterCenters )
{
    vector<float> hist1(clusterCenters.rows,0), hist2(clusterCenters.rows,0);

    float distFromCluster, minDis=999 ;

    if (desc1.rows <= clusterCenters.rows || desc2.rows <= clusterCenters.rows)
        return 0;

    for (int i = 0; i < desc1.rows; i++)
    {
        int index = -1;
        minDis = 999;
        for (int j = 0; j < clusterCenters.rows; j++)
        {
            distFromCluster = norm(desc1.row(i) - clusterCenters.row(j));

            if (distFromCluster < minDis)
            {
                minDis = distFromCluster;
                index = j;
            }
        }
        hist1[index]++;
    }

    for (int i = 0; i < desc2.rows; i++)
    {
        int index = -1;
        minDis = 999;
        for (int j = 0; j < clusterCenters.rows; j++)
        {
            distFromCluster = norm(desc2.row(i) - clusterCenters.row(j));

            if (distFromCluster < minDis)
            {
                minDis = distFromCluster;
                index = j;
            }
        }
        hist2[index]++;
    }

    Scalar sum1 = sum(hist1);
    Scalar sum2 = sum(hist2);

    for(int k=0; k<hist1.size(); k++)
    {
        hist1[k] /= sum1[0];
        hist2[k] /= sum2[0];
    }


    double bhattCoeff = 0;

    for(int k=0; k<hist1.size(); k++)
    {
        bhattCoeff += sqrt(hist1[k]*hist2[k]);
    }

    double hellingerDistance = sqrt(1-bhattCoeff);

    return hellingerDistance;


}
