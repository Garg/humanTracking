A novel SURF-based algorithm for tracking human from mobile robot platform.

  - Introduction:
	 -- Detecting and tracking a human from a mobile
	    robot platform has several applications in service robotics where
	    a robot is expected to assist humans. In this paper, we propose
	    a novel interest point-based algorithm that can track a human
	    reliably under several challenging situations like variation in
	    illumination, pose change, scaling, camera motion and occlusion.
	    The limitations of point-based methods are overcome using colour
	    information and imposing a structure on the colour blobs. When-
	    ever sufficient number of SURF matching points are not available
	    for a given frame, the presence of human is detected using Markov
	    random field based graph matching algorithm. Imposition of
	    structure on coloured blobs helps in eliminating background
	    objects having similar colour distribution. The stability-versus-
	    plasticity dilemma inherent in a tracking over long run is resolved
	    by selecting new templates on-line and maintaining a fixed-size
	    tree of templates which is updated with new information. The
	    performance of the algorithm is demonstrated through simulation
	    on standard datasets and the computation time is found to be
	    comparable with existing SURF-based tracking methods.

  - Tested with following requirements
    	 -- Ubuntu 14.04
	 -- OpenCv 2.4.9
    	 -- LibDAI 0.3.2 - This is used as an external library. The path for the same needs to be set in CMakeLists.txt. This is required for
    	    graph matching using MRF-MAP based solution.
	    Use this link - https://staff.fnwi.uva.nl/j.m.mooij/libDAI/ to install libDAI from its Readme instructions
  - Compile
    	 -- cd build
    	 -- cmake ..
         -- make
  - Run
    	 -- cd bin
    	 -- ./humanTracking
    		--- Select the boundary of human to be tracked by left mouse clicking to form a polygon around human
    		--- Right click when selection completes
  - Sample Input
	    Input video /bin/d.avi. The initial selection is already set in the code, which can be replaced by mouse based
	    selection of the human body. After that, the segmented body image needs three left clicks to specify that we are using
	    the code in the full human mode. If only torso and legs are tracked, then two left clicks and one right click is
	    required along with the corresponding macro set to 1.
  - Output
    The output tracked video is stored in /bin/trackVideo.avi. Some data files are generated as well.


Please contact at garg.sourav_at_tcs.com / sourav.sahaji_at_gmail.com for queries.


